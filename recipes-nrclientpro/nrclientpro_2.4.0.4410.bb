SUMMARY = "NeoRouter VPN Client (64bit) "
DESCRIPTION = "This recipe adds NeoRouter VPN Client (binary) to system"
LICENSE = "Proprietary"
PACKAGE_ARCH = "x86_64"
RDEPENDS_${PN} = "bash util-linux-libuuid"

LIC_FILES_CHKSUM = "file://License.txt;subdir=nrclientpro;md5=5b78a97bd31078594d05d71bdc3e5f9f"

# skip certain insane QA tests for binaries

INSANE_SKIP_${PN} = "already-stripped ldflags dev-so file-rdeps"

SKIP_FILEDEPS_${PN} = "1"

# set source dir to subdir

S = "${WORKDIR}/nrclientpro/"

SRC_URI = "file://nrclient-2.4.0.4410-pro-fedora-x86_64.rpm;subdir=nrclientpro"
SRC_URI[md5sum] = ""
SRC_URI[sha256sum] = ""

# Common variable and task for the binary package recipe.
# Basic principle:
# * The files have been unpacked to ${S} by base.bbclass
# * Skip do_configure and do_compile
# * Use do_install to install the files to ${D}
#
# Note:
# The "subdir" parameter in the SRC_URI is useful when the input package
# is rpm, ipk, deb and so on, for example:
#
# SRC_URI = "http://foo.com/foo-1.0-r1.i586.rpm;subdir=foo-1.0"
#
# Then the files would be unpacked to ${WORKDIR}/foo-1.0, otherwise
# they would be in ${WORKDIR}.
#

# Skip the unwanted steps
do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install () {
    [ -d "${S}" ] || (echo missing source && exit 1)
    cd ${S} || (echo unable to change to && exit 1)
    tar --no-same-owner --exclude='./etc' --exclude='./patches' --exclude='./.pc' -cpvf - . \
        | tar --no-same-owner -xpvf - -C ${D}
}

FILES_${PN} = " ${bindir}/nrclient.jar ${bindir}/nrclient.sh ${bindir}/nrclientcmd ${bindir}/nrservice /usr/local/ZebraNetworkSystems/NeoRouter/nrclient.README /usr/local/ZebraNetworkSystems/NeoRouter/nrservice.service /usr/local/ZebraNetworkSystems/NeoRouter/nrservice.sh"

INHIBIT_PACKAGE_DEBUG_SPLIT = "1"

