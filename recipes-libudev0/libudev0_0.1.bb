SUMMARY = "libudev.so.0 link for statically built binaries"
DESCRIPTION = ""
LICENSE = "MIT"
PACKAGE_ARCH = "x86_64"
DEPENDS_${PN} = "systemd"

LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=4d92cd373abda3937c2bc47fbc49d690 \
                    file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

# files to be installed in the user directory

#SRC_URI = "file://file1 \
#           file://file2 \
#           file://file3 \
#           file://file4"
SRC_URI = "file://libudev.so.0"
SRC_URI[md5sum] = ""
SRC_URI[sha256sum] = ""

S = "${WORKDIR}/${PN}-${PV}"

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install () {
	mkdir -p ${D}${libdir}
	cd ${D}${libdir}
	ln -sf libudev.so.1 libudev.so.0
}

FILES_${PN} += "${libdir}/libudev.so.0 ${libdir}/*.so"
FILES_SOLIBSDEV = ""

# Prevents do_package failures with:
# debugsources.list: No such file or directory:
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
