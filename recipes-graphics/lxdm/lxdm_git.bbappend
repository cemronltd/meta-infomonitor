FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

do_compile_append() {
    # default background configured not available / no password field available / no default screensaver
#    sed -i     -e 's,bg=,# bg=,g' \
#        -e 's,# skip_password=,skip_password=,g' \
#        -e 's,# arg=.*,arg=${bindir}/X -s 0,g' \
#        ${S}/data/lxdm.conf.in
    # add default configuration
#    oe_runmake -C ${B}/data lxdm.conf
}

do_install_append() {
    sed -i -e '/# autologin=/a autologin=infomonitor' \
        ${D}${sysconfdir}/lxdm/lxdm.conf
}

RCONFLICTS_${PN} += "xserver-nodm-init"
RREPLACES_${PN} += "xserver-nodm-init"
RCONFLICTS_${PN} += "xserver-nodm-init-systemd"
RREPLACES_${PN} += "xserver-nodm-init-systemd"

