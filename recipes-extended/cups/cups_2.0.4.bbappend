
do_install_append() {
  echo "Deleting unneeded cups files from " ${D}${systemd_unitdir}/system/
  rm -f ${D}${systemd_unitdir}/system/org.cups.cupsd.socket
  rm -f ${D}${systemd_unitdir}/system/org.cups.cups-lpd.socket
  rm -f ${D}${systemd_unitdir}/system/org.cups.cupsd.path
  rm -f ${D}${systemd_unitdir}/system/org.cups.cupsd.service
  rm -f ${D}${systemd_unitdir}/system/org.cups.cups-lpd@.service
}
