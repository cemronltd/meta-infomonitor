SUMMARY = "myinfomonitor Player"
DESCRIPTION = "This recipe adds the myinfomonitor Player (standalone)"
LICENSE = "Commercial"
LIC_FILES_CHKSUM = "file://LICENSE;md5=cac58c74a463270cc1640dcc1e6c4874"

inherit qmake5

PACKAGECONFIG_append_pn-qtbase = " accessibility "

DEPENDS="qtwebengine qtdeclarative qtmultimedia qtav"
RDEPENDS_${PN}=" \
	qtav \
	qt3d \
	qt3d-qmlplugins \
	qtconnectivity \
	qtconnectivity-qmlplugins \
	qtdeclarative \
	qtdeclarative-plugins \
	qtdeclarative-qmlplugins \
	qtimageformats \
	qtimageformats-plugins \
	qtlocation \
	qtlocation-plugins \
	qtlocation-qmlplugins \
	qtmultimedia \
	qtmultimedia-plugins \
	qtmultimedia-qmlplugins \
	qtquickcontrols-qmlplugins \
	qtsensors \
	qtsensors-qmlplugins \
	qtserialport \
	qtsvg \
	qtsvg-plugins \
	qtsystems-qmlplugins \
	qtwebengine \
	qtwebengine-qmlplugins \
	qtwebsockets-qmlplugins \	
"

#SRC_URI = "file://file1 \
#           file://file2 \
#           file://file3 \
#           file://file4"
#SRCREV = "6156a0837ef26d13a858e2a2e0df4b064dfc3833"
SRCREV = "7c71bdd6f0f5102a43646c7af860db8ce334bb9a"

PV = "6.0+git${SRCPV}"

SRC_URI = " \
	git://git@bitbucket.org:/cemronltd/infomonitor-player-qt.git;protocol=ssh;branch=master \
	file://LICENSE;subdir=git \
	"

SRC_URI[md5sum] = "787a01c610405eaf4cc71df840bbff5c"
SRC_URI[sha256sum] = "2e8fccdd1dff93abf15182bf79a20a11cd92545baa06930fb2f1162f0f14671c"

S = "${WORKDIR}/git"

do_install () {	
	install -d ${D}/opt/infomonitor/player/bin
	install -m 0755 ${B}/infomonitorplayer ${D}/opt/infomonitor/player/bin/	
	cd ${D}${libdir}
	ln -s /opt/google/chrome/PepperFlash .	
}

FILES_${PN} = "/opt/infomonitor/player /usr/lib/PepperFlash/*"

# Prevents do_package failures with:
# debugsources.list: No such file or directory:
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
