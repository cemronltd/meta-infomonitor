SUMMARY = "InfoMonitor Player Watcher"
DESCRIPTION = "Watchdog for InfoMonitor Player"
LICENSE = "Commercial"

LIC_FILES_CHKSUM = ""

RDEPENDS_${PN}="bash procps xdg-utils python python-psutil python-imaging python-cheetah xwininfo libffi freetype python-email python-json python-netserver python-logging python-netclient python-core python-io python-pygtk python-twisted python-pyserial python-ctypes python-wsgiref"

INSANE_SKIP_${PN} = "already-stripped"

SRC_URI = "file://InfoMonitorWatcher.tar.gz \
	   file://InfoMonitorWatcher.cfg \
	   file://infomonitorwatcher.service"

SRC_URI[md5sum] = ""
SRC_URI[sha256sum] = ""

S = "${WORKDIR}"

inherit autotools

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install () {	
	mkdir -p ${D}/opt/infomonitor
	cd ${S}
	tar -v --no-same-owner --exclude='./patches' --exclude='./.pc' -cpvf - watcher/ \ | tar --no-same-owner -xpvf - -C ${D}/opt/infomonitor	
	install -m 0755 ${S}/InfoMonitorWatcher.cfg ${D}/opt/infomonitor/watcher/
	mkdir -p ${D}/etc/systemd/system	
	install -m 0755 ${S}/infomonitorwatcher.service ${D}/etc/systemd/system/
	mkdir -p ${D}/etc/systemd/system/graphical.target.wants
	ln -sf /etc/systemd/system/infomonitorwatcher.service ${D}/etc/systemd/system/graphical.target.wants/infomonitorwatcher.service

}

FILES_${PN} = "/opt/infomonitor/watcher /etc/systemd/system/infomonitorwatcher.service /etc/systemd/system/graphical.target.wants/infomonitorwatcher.service"

# Prevents do_package failures with:
# debugsources.list: No such file or directory:
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
