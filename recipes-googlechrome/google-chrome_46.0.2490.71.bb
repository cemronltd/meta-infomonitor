SUMMARY = "Google Chrome (without auto updates, 64bit) "
DESCRIPTION = "This recipe adds Google Chrome (binary) to system"
LICENSE = "Proprietary"
PACKAGE_ARCH = "x86_64"
RDEPENDS_${PN} = "bash udev libudev systemd openssl libselinux krb5 xz libpcap cairo expat freetype libxcursor libxtst glib-2.0 nss libxi \
		  fontconfig gdk-pixbuf libcap libxdamage libxext libxrandr alsa-lib dbus gtk+ gconf pango nspr \
		  libx11 libxcomposite libxrender libxfixes libxscrnsaver cups-lib"


LIC_FILES_CHKSUM = "file://eula_text.html;subdir=google-chrome;md5=a4fcf95699bbb37e9522695b0d632e0b"

# skip certain insane QA tests for binaries

INSANE_SKIP_${PN} = "already-stripped ldflags dev-so file-rdeps"

SKIP_FILEDEPS_${PN} = "1"

# set source dir to subdir

S = "${WORKDIR}/google-chrome/"

SRC_URI = "file://google-chrome-stable_46.0.2490.71_x86_64.rpm;subdir=google-chrome"
SRC_URI[md5sum] = ""
SRC_URI[sha256sum] = ""

# Common variable and task for the binary package recipe.
# Basic principle:
# * The files have been unpacked to ${S} by base.bbclass
# * Skip do_configure and do_compile
# * Use do_install to install the files to ${D}
#
# Note:
# The "subdir" parameter in the SRC_URI is useful when the input package
# is rpm, ipk, deb and so on, for example:
#
# SRC_URI = "http://foo.com/foo-1.0-r1.i586.rpm;subdir=foo-1.0"
#
# Then the files would be unpacked to ${WORKDIR}/foo-1.0, otherwise
# they would be in ${WORKDIR}.
#

# Skip the unwanted steps
do_configure[noexec] = "1"
do_compile[noexec] = "1"

# Install the files to ${D}
do_install () {
    [ -d "${S}" ] || (echo missing source && exit 1)
    cd ${S} || (echo unable to change to && exit 1)
    tar --no-same-owner --exclude='./etc' --exclude='./patches' --exclude='./.pc' -cpvf - . \
        | tar --no-same-owner -xpvf - -C ${D}
    cd ${D}
    ln -sf lib lib64
    cd usr
    ln -sf lib lib64
    cd ${D}/opt/google/chrome
    ln -sf ../../../lib/libudev.so.1 libudev.so.0
}



FILES_${PN} = "${bindir}/google-chrome-stable /lib64 /usr/lib64 ${bindir}/google-chrome /opt/google/chrome ${datadir}/applications ${datadir}/gnome-control-center/default-apps"

