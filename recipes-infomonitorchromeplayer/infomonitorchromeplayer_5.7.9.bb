SUMMARY = "InfoMonitor Player for Google Chrome"
DESCRIPTION = "This recipe adds InfoMonitor Player for Google Chrome"
LICENSE = "Commercial"

LIC_FILES_CHKSUM = ""

DEPENDS="google-chrome"
RDEPENDS_${PN}="google-chrome"
# files to be installed in the user directory

#SRC_URI = "file://file1 \
#           file://file2 \
#           file://file3 \
#           file://file4"
SRC_URI = "file://bfjejalogdnipfknpnnbdclghhhbocea_main.crx \
	   file://bfjejalogdnipfknpnnbdclghhhbocea.json"
SRC_URI[md5sum] = "787a01c610405eaf4cc71df840bbff5c"
SRC_URI[sha256sum] = "2e8fccdd1dff93abf15182bf79a20a11cd92545baa06930fb2f1162f0f14671c"

S = "${WORKDIR}"

inherit autotools

do_configure[noexec] = "1"
do_compile[noexec] = "1"

do_install () {	
        [ ! -f ${D}/opt/google/chrome/default_apps ] && mkdir -p ${D}/opt/google/chrome/extensions	
        cp ${S}/bfjejalogdnipfknpnnbdclghhhbocea_main.crx ${D}/opt/google/chrome/extensions/infomonitorplayer.crx
	install -m 0755 ${S}/bfjejalogdnipfknpnnbdclghhhbocea.json ${D}/opt/google/chrome/extensions/
  
}

FILES_${PN} = "/opt/google/chrome/extensions"

# Prevents do_package failures with:
# debugsources.list: No such file or directory:
INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
