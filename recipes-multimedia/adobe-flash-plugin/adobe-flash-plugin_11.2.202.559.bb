DESCRIPTION = "Adobe Flash NPAPI plugin for Mozilla/Firefox web browsers"
LICENSE = "Proprietary"
PACKAGE_ARCH = "x86_64"

RDEPENDS_${PN} = "bash libxcursor freetype nss libxext nspr \
			      cairo libx11 libxau atk fontconfig libice \
			      glib-2.0 libxdmcp libxt gdk-pixbuf gtk+ pango \
			      libxft libsm libxrender"

# skip certain insane QA tests for binaries

INSANE_SKIP_${PN} = "already-stripped ldflags dev-so file-rdeps"

SKIP_FILEDEPS_${PN} = "1"

# skip online loading of plugin for now
# SRC_URI = "https://fpdownload.adobe.com/get/flashplayer/pdc/${PV}/flash-plugin-${PV}-release.x86_64.rpm"
# SRC_URI[md5sum] = "f7aec1fa0464370c7ca12452f93cad07"
# SRC_URI[sha256sum] = "992bd1644a54e802c2834b81738e22b443504b6802e73e8bb4df16df4fc0f795"

# set source dir to subdir

AFSUBDIR = "adobe_flash_plugin"

S = "${WORKDIR}/${AFSUBDIR}/"


SRC_URI = "file://flash-plugin-11.2.202.559-release.x86_64.rpm;subdir=${AFSUBDIR}"
SRC_URI[md5sum] = ""
SRC_URI[sha256sum] = ""

LIC_FILES_CHKSUM = "file://usr/lib64/flash-plugin/LICENSE;subdir=${AFSUBDIR};md5=0ea2b3cc4cd2140aeb44b221d7a978c6"


# Skip the unwanted steps
do_configure[noexec] = "1"
do_compile[noexec] = "1"

# Install the files to ${D}
do_install () {
    [ -d "${S}" ] || (echo missing source && exit 1)
    cd ${S} || (echo unable to change to && exit 1)
    echo ${S}
    if [ -d "usr/lib64" ]; then
      mv usr/lib64 usr/lib
    fi
    tar --no-same-owner --exclude='./etc' --exclude='./patches' --exclude='./.pc' -cpvf - . \
        | tar --no-same-owner -xpvf - -C ${D}
    cd ${D}
    ln -sf lib lib64
    cd usr
    ln -sf lib lib64
}



FILES_${PN} = "${bindir}/flash-player-properties \
			 /lib64 \
			 /usr/lib64 \
			 ${libdir}/flash-plugin/* \
			 ${libdir}/kde4/* \
			 ${datadir}/applications/* \
			 ${datadir}/kde4/services/* \
			 ${datadir}/icons/hicolor/*"


#do_configure() {
#        rpm=${WORKDIR}/flash-plugin-${PV}-release.x86_64.rpm
#        if [ -f "$rpm" ]; then
#                rpm2cpio.pl $rpm | cpio -id
#                find . -type f -exec mv -i {} "`pwd`" \;
#        fi
#}
#do_install() {
#        mkdir -p ${D}${libdir}/mozilla/plugins/
#        install -m 0755 libflashplayer.so ${D}${libdir}/mozilla/plugins/
#}

#FILES_${PN} = "${libdir}/* ${libdir}/X11/*"
