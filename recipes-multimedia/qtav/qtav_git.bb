SUMMARY = "QtAV"
DESCRIPTION = "A multimedia playback framework based on Qt + FFmpeg. Cross platform. High performace. Easy to use and develop."

LICENSE = "LGPLv2.1+ & GPLv3+"
LIC_FILES_CHKSUM = "file://gpl-3.0.txt;md5=d32239bcb673463ab874e80d47fae504 \
                    file://lgpl-2.1.txt;md5=4fbd65380cdd255951079008b364516c"

inherit qmake5


PREFERRED_VERSION_libav ?= "9.18"

DEPENDS="virtual/libgl pulseaudio libva libxv libass qtdeclarative ffmpeg"
RDEPENDS_${PN}=""

SRCREV = "933b082d8e7f15055d8e3fefd7ec6aa1223b2113"

PV = "1.9.0+git${SRCPV}"

SRC_URI = "git://github.com/wang-bin/QtAV.git;branch=master"

S = "${WORKDIR}/git"


PACKAGES += "libqtav libqtavwidgets libqtavwidgets-mkspecs qml-module-qtav qtav-players libqtav-staticdev libqtav-mkspecs "

RRECOMMENDS_libqtav ?= " libva libva-intel-driver "

DEPENDS_libqtavwidgets = "libqtav"
DEPENDS_qtav-players = "libqtav libqtavwidgets qml-module-qtav qtsvg"


FILES_libqtav = "${libdir}/libQtAV.so.* \
		 ${libdir}/libQtAV.prl"

FILES_libqtav-mkspecs = "${OE_QMAKE_PATH_QT_ARCHDATA}/mkspecs/features/* \
		 				 ${OE_QMAKE_PATH_QT_ARCHDATA}/mkspecs/features/av.prf \
		 				 ${OE_QMAKE_PATH_QT_ARCHDATA}/mkspecs/modules/qt_lib_av.pri \  
  		 				 ${OE_QMAKE_PATH_QT_ARCHDATA}/mkspecs/modules/qt_lib_av_private.pri"

FILES_libqtav-staticdev = "${libdir}/libcommon.a \
  		 	               ${libdir}/libcommon.prl"
  
FILES_libqtavwidgets = "${libdir}/libQtAVWidgets.so.* \
			            ${libdir}/libQtAVWidgets.prl "
			
FILES_libqtavwidgets-mkspecs = " ${OE_QMAKE_PATH_QT_ARCHDATA}/mkspecs/features/avwidgets.prf \
								 ${OE_QMAKE_PATH_QT_ARCHDATA}/mkspecs/modules/qt_lib_avwidgets.pri \
							     ${OE_QMAKE_PATH_QT_ARCHDATA}/mkspecs/modules/qt_lib_avwidgets_private.pri"

FILES_qml-module-qtav = "${libdir}/qt5/qml/QtAV/*"

FILES_qtav-players = "${bindir}/QMLPlayer \
					  ${bindir}/player \ 
					  ${datadir}/icons/hicolor/* \
					  ${datadir}/applications/* \
					  "
						   
do_install() {
    qmake5_base_do_install
    install -d ${D}/${OE_QMAKE_PATH_QT_ARCHDATA}/mkspecs
    install -d ${D}/${OE_QMAKE_PATH_QT_ARCHDATA}/mkspecs/features
    install -d ${D}/${OE_QMAKE_PATH_QT_ARCHDATA}/mkspecs/modules
    ln -s ${D}/${libdir}/libQtAV.so ${D}/${libdir}/libQt5AV.so
    ln -s ${D}/${libdir}/libQtAVWidgets.so ${D}/${libdir}/libQt5AVWidgets.so
	mv ${D}/usr/mkspecs/features/* ${D}/${OE_QMAKE_PATH_QT_ARCHDATA}/mkspecs/features/
	mv ${D}/usr/mkspecs/modules/* ${D}${OE_QMAKE_PATH_QT_ARCHDATA}/mkspecs/modules/
	mv ${D}${bindir}/libcommon.* ${D}${libdir}
	rm -rf ${D}/usr/mkspecs
}