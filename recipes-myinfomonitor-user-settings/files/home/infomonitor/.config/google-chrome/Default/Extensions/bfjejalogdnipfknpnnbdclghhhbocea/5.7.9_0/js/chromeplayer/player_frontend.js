    var player;
    var inited = false;
    var windowWidth = 0;
    var windowHeight = 0;

    function execFunc(functionName, context, cargs) {
        var args = Array.prototype.slice.call(arguments).splice(2);
        var namespaces = functionName.split(".");
        var func = namespaces.pop();
        for (var i = 0; i < namespaces.length; i++) {
            context = context[namespaces[i]];
        }
        return context[func].apply(this, args);
    }
    var port_fe = chrome.extension.connect({name: "im.ports.chromeplayer.frontend"});

    port_fe.onMessage.addListener(function (msg) {
        if (msg.rpc) {
            execFunc(msg.rpc.func, window, msg.rpc.args);
        } else if (msg.event) {
            switch (msg.event.name) {
                case "imPlaybackDurationChange":
                    player.onPlaybackDurationChange(msg.event.details);
                    break;
                case "imPlaybackComplete":
                    player.imPlaybackComplete(msg.event.details);
                    break;
                case "imPlaybackPause":
                    player.onPlaybackPause(msg.event.details);
                    break;
                case "imPlaybackStart":
                    player.onPlaybackStart(msg.event.details);
                    break;
            }
        }
    });

    function postMessage(msg) {
        if (typeof port_fe == 'undefined') {
            /*   setTimeout(function() {
             postMessage(msg);
             },1000);  */
            console.log("port not available");
            return;
        }
        port_fe.postMessage(msg);
    }

    // ###########



    function IMChromePlayerFrontend() {
        var config;
        var rotationPaused = false;
        var currentSlide = null;
        var defaultDuration = 10000;
        var slideLoadCount = 0;
        var slideCount = 0;
        var loadComplete = false;
        var self = this;
        var staleSlides = [];
        var lastSlideChangeTimestamp = null;
        var loadingConfig = false;
        var firstSlideIndex = -1;
        var networkOnline = true;
        var firstConfigLoadInSession = true;
        var disableSlideHibernate = false;

        var timeTableChecker = null;

        this.setNetworkStatus = function (isOnline) {
            networkOnline = isOnline;
        };

        this.playing = function () {
            return !rotationPaused;
        };
        this.paused = function () {
            return rotationPaused;
        };
        this.incrementLoadCount = function () {
            slideLoadCount++;
            console.log("first load in session:", firstConfigLoadInSession);
            if (player.firstConfigLoadInSession) {
                $("#im-cp-message").css("display", "block").empty().append("Loading " + slideLoadCount + "/" + slideCount);
                if (slideLoadCount == slideCount) {
                    var msg = $("#im-cp-message").css("display", "block").empty().append("Please wait... (" + slideLoadCount + "/" + slideCount + ")");
                    msg.append("<br><br>Network status: " + (networkOnline ? "Online" : "Offline"));
                }
            }
            if (slideLoadCount == slideCount) {
                self.onLoadComplete();
            }

        };
        this.getLoadCount = function () {
            return slideLoadCount;
        };
        this.getLoadComplete = function () {
            return loadComplete;
        };
        this.setLoadComplete = function (lc) {
            loadComplete = lc;
        };
        this.removeSlide = function (details) {
            if (details.url) {
                var f = $(".im-cp-slide").find("iframe[imcontentsrc='" + cleanUpSlideURL(details.url) + "']");
                if (f.length > 0) {
                    console.log("Removing slide:", cleanUpSlideURL(details.url));
                    if (slideEquals(currentSlide, f.parent())) {
                        currentSlide = null;
                    }
                    f.parent().remove();


                    slideCount--;

                } else {
                    console.log("Target frame for removal not found", details);
                }
            }
        };
        this.onLoadComplete = function () {
            self.setLoadComplete(true);
            player.firstConfigLoadInSession = false;
            console.log("iframe load complete, first slide", self.getStartupSlide());
            //$("#im-cp-message").css("display","none").empty();
            if (!currentSlide || typeof(currentSlide) === 'undefined') {
                currentSlide = self.getStartupSlide();

                // let's prepare first slide and wait things to settle for 2000ms
                self.prepareSlide(currentSlide, function () {
                    showSlide(currentSlide);
                    self.sendSlideChangedMessage();
                }, 1000);
            }
        };
        this.getStartupSlide = function () {
            /*if(firstSlideIndex && firstSlideIndex>0) {
             try {
             var slide=$($(".im-cp-slide")[firstSlideIndex]);
             console.log("Different startupslide", firstSlideIndex);
             if(slide && slide.length>0) return slide;
             } catch (e) {

             }

             } */
            return $(".im-cp-slide:first");
        };
        this.setStartupSlide = function (idx) {
            if (idx)
                firstSlideIndex = idx;
        };
        this.start = function () {
            self = this;
            this.requestLoadConfig();
        };
        this.requestLoadConfig = function (callback) {
            postMessage({rpc: {func: "player.sendConfig"}});
            if (callback) callback();
        };

        this.loadConfig = function (newConfig) {
            console.log("loadConfig called");
            if (loadingConfig) return;
            loadingConfig = true;

          //  try {
                if (!player.firstConfigLoadInSession) {
                    $("#im-cp-message").css("display", "none");

                }
                reloadZones();
                if (!currentSlide) {
                    console.log("Loading configuration", newConfig);
                    $(".im-cp-slide").remove();
                    slideLoadCount = 0;
                    self.setLoadComplete(false);
                    var i = 0;
                    slideCount = newConfig.slides.length;
                    for (var slide in newConfig.slides) {
                        var conf = newConfig.slides[slide];
                        if(!disableSlideHibernate) {
                            disableSlideHibernate=typeof(conf["showon"])!=='undefined';
                        }
                        player.createSlide(conf.url, {duration: conf.viewtime, overwrite: false, index: i, timeTable: conf["showon"]});
                        i++;
                    }
                    if (slideCount == 1 && typeof(conf["showon"])==='undefined') {
                        console.log("1 slide, multiplying it to enable prefetch");
                        var conf = newConfig.slides[0];
                        player.createSlide(conf.url + "&imidx=1", {duration: conf.viewtime, overwrite: false, index: 1, timeTable: conf["showon"]});
                        player.createSlide(conf.url + "&imidx=2", {duration: conf.viewtime, overwrite: false, index: 2, timeTable: conf["showon"]});
                        slideCount = 3;
                    }
                    if (slideCount == 2 && typeof(conf["showon"])==='undefined') {
                        console.log("2 slides, multiplying them to enable prefetch");
                        var conf0 = newConfig.slides[0];
                        var conf1 = newConfig.slides[1];
                        player.createSlide(conf0.url + "&imidx=1", {duration: conf0.viewtime, overwrite: false, index: 2, timeTable: conf["showon"]});
                        player.createSlide(conf1.url + "&imidx=2", {duration: conf1.viewtime, overwrite: false, index: 3, timeTable: conf["showon"]});
                        slideCount = 4;
                    }

                } else {
                    console.log("Reloading due to configuration change", newConfig);
                    window.location.reload();
                }
                $(".im-cp-slide").removeAttr("imvalid");
                if ($(".im-cp-slide").length == 0) {
                    $("#im-cp-message").empty().css("display", "block").append("Please add content to the screen!");

                }
           // } catch (e) {
           //     console.log("BUG: Exception caught", e);
           // }
            player.firstConfigLoadInSession = false;
            loadingConfig = false;

        };
        this.prepareSlide = function (slide, callback, callbackDelay, force) {
            if(disableSlideHibernate && !force) return;
            if (!slide) {
                console.log("Cannot prepare undefined slide");
                return;
            }
            var f = slide.find('iframe');
            if (f && f.length > 1) {
                $(f[0]).remove();
            }
            if (f && (!f.attr('src') || f.attr('src') === 'about:blank')) {
                f.css("display","inline-block");
                f.attr('src', self.prepareURL(f.attr('imcontentsrc'), f.parent().attr("imduration")));
                if (typeof(callback) == 'function') {
                    setTimeout(function () {
                        callback();
                    }, callbackDelay);
                }
            }

        };
        this.hibernateSlide = function (slide) {
            if(disableSlideHibernate) return;
            var f = slide.find('iframe');
            if (f && f.length > 1) {
                $(f[0]).remove();
            }
            if (f) {
                //console.log("Hibernating frame:",f.attr("imcontentsrc"));
                f.attr('src', 'about:blank');
            }
        };
        this.reloadFrame = function (details) {
            console.log("player front end: reloadFrame", details);
            var f = $(".im-cp-slide").find("iframe[imcontentsrc='" + cleanUpSlideURL(details.url) + "']");
            if (f.length > 0) {
                console.log("Reloading frame:", cleanUpSlideURL(details.url), details);
                f.removeAttr("src");
                f.attr("src", self.prepareURL(details.url));
            } else {
                console.log("Target frame for reloading not found", details);
            }
        };
        this.reloadCurrentSlide = function () {
            if (currentSlide) {
                self.sendPlayerControlMessage("ReloadCurrentSlide", {URL: currentSlide.find("iframe").attr("imcontentsrc"), duration: currentSlide.attr("imduration")});
            }
        };
        this.prepareURL = function (url, duration, isReload) {
            return addParameters(cleanUpSlideURL(url), windowWidth, windowHeight, duration);
        };
        var addParameters = function (url, w, h, d) {
            if (typeof(url) === 'undefined' || !url) return;
            if (url.indexOf('?') == -1)
                url += "?";
            else
                url += "&";
            url += "w=";
            url += w;
            url += "&h=";
            url += h;
            //   url += "&d=";
            //   url += (d?d:"10001");
            return url;

        };
        var cleanUpSlideURL = function (url) {
            if (typeof(url) === 'undefined' || !url) return;
            url = url.replace(/[?&]w=[0-9]*/, '');
            url = url.replace(/[?&]h=[0-9]*/, '');
            url = url.replace(/[?&]d=[0-9]*/, '');
            url = url.replace(/[?&]__im_offline_cache_id=[a-zA-Z0-9]*/, '');
            return url;

        };
        var slideEquals = function (s1, s2) {
            var f1 = s1.find("iframe");
            var f2 = s2.find("iframe");
            if (f1.length == 0 || f2.length == 0) return false;
            return (cleanUpSlideURL(f1.attr("imcontentsrc")) === cleanUpSlideURL(f2.attr("imcontentsrc")));
        };
        var slideURLEquals = function (url1, url2) {
            return (cleanUpSlideURL(url1) === cleanUpSlideURL(url2));

        };
        this._slideEquals = function (s1, s2) {
            var f1 = s1.find("iframe");
            var f2 = s2.find("iframe");
            if (f1.length == 0 || f2.length == 0) return false;
            return (cleanUpSlideURL(f1.attr("imcontentsrc")) === cleanUpSlideURL(f2.attr("imcontentsrc")));
        };
        this.updateSlideDuration = function (args) {
            if (typeof(args.duration) === 'undefined' || typeof(args.url) === 'undefined') {
                console.log("updateSlideDuration failed, invalid parameters,", args);
            }
            var frames = $("li > iframe");
            if (args.duration < 1000) {
                console.log("Duration is under 1000ms, skipping metadata update");
                return;
            }
            for (var i = 0; i < frames.size(); i++) {
                var f = $(frames[i]);
                if ((f.attr('imcontentsrc') === cleanUpSlideURL(args.url))) {
                    //console.log("Duration change to ",args.duration ,"ms, url:", args.url, "all params:", args);
                    f.parent().attr('imduration', args.duration);
                    break;
                }
            }
        };

        this.callBackendFunction = function (func, args) {
            postMessage({rpc: {func: func, args: args}});
        };
        this.startTimeTableChecker = function() {
            if(timeTableChecker) clearInterval(timeTableChecker);

            console.log("Timetable checker activated");
            timeTableChecker=setInterval(function(){IM_checkTimeTableAndRotate()},1000);
        }
        this.setTimeTable = function (target, timeTable) {
            var secs = timeTable["seconds"];
            var minutes = timeTable["minutes"];
            var hours = timeTable["hours"];
            target.setAttribute("imtimetableseconds",typeof(secs) != 'undefined' && secs.length>0 ? secs : null);
            target.setAttribute("imtimetableminutes",typeof(minutes) != 'undefined' && minutes.length>0 ? minutes : null);
            target.setAttribute("imtimetablehours",typeof(hours) != 'undefined' && hours.length>0 ? hours : null);
            console.log("set",target,secs,minutes,hours,target["imtimetableseconds"]);
            if(!timeTableChecker)this.startTimeTableChecker();
        }
        /*
         options.duration = view duration in ms
         options.overwrite = overwrite existing slide
         options.index = index to append/insert slide to
         options.parent = parent obj (optional),
         options.viewer = viewer obj (optional),
         options.isVisible = slide visible (optional),
         options.timeTable = array of show times

         */
        this.createSlide = function (url, options) {
            console.log(options, options["duration"]);
            if (options["overwrite"]) {
                $(".im-cp-slide").remove();
            }
            var c = options["viewer"];
            if (!c) {
                c = document.createElement("iframe");
                c.addEventListener("load", function (evt) {
                    if (!player.getLoadComplete()) {
                        player.incrementLoadCount();
                    }
                });
                c.setAttribute("imcontentsrc", url);
                c.setAttribute("class", "im-cp-viewer");

                c.style.height = windowHeight;
                c.style.width = windowWidth;
            }
            if (typeof(options["parent"])!='undefined') {
                var p=options["parent"];
                p.setAttribute("imduration", options["duration"]);
                if (options["timeTable"] != "undefined") this.setTimeTable(p, options["timeTable"]);

                p.setAttribute("immodified", "true");
                p.style.top = (windowHeight + 1000) + "px";
                console.log("Using old li", p);
                p.appendChild(c);
                if (typeof(options["timeTable"]) != 'undefined') {
                    this.setTimeTable(p, options["timeTable"]);
                    this.prepareSlide($(p),null,null,true);
                }
            } else {
                var b = $(".im-cp-showcontainer");
                var elem = document.createElement("li");

                elem.setAttribute("class", "im-cp-slide");
                elem.setAttribute("imduration", options["duration"]);
                elem.setAttribute("imvalid", "true");

                elem.style.visibility = (options["isVisible"] ? "visible" : "hidden");
                elem.style.zIndex = -1;
                //c.style.zIndex = -1;
                c.style.visibility = elem.style.visibility;
                c.style.position = "relative";
                elem.appendChild(c);
                if (typeof(options["timeTable"]) != 'undefined') {
                    this.setTimeTable(elem, options["timeTable"]);
                    this.prepareSlide($(elem),null,null,true);
                }
                var currentSlideInPos = b.find(".im-cp-slide")[options["index"]];
                if (typeof(currentSlideInPos) === 'undefined') {
                    b[0].appendChild(elem);
                } else {
                    console.log("inserting before", currentSlideInPos);
                    $(elem).insertBefore(currentSlideInPos);
                }
            }
        };
        this.initCurrentSlide = function () {
            currentSlide = $(".im-cp-slide:first");
            this.prepareSlide(currentSlide);

        };
        var hideAndUnPrepareSlide = function (slideObj) {
            hideSlide(slideObj);
            slideObj.find("iframe").removeAttr("src").css("display", "none").css("visibility","hidden");

        };
        var hideSlide = function (slideObj) {
            slideObj.css("top", (windowHeight + 1000) + "px");
            slideObj.css("visibility", "hidden");
            slideObj.css("z-index", "-1");

        }

        var showSlide = function (slideObj) {
            slideObj.css("top", "0");
            slideObj.css("visibility", "visible").css("z-index", "1000");
            slideObj.find("iframe").css("display", "inline-block").css("visibility","visible");
        };

        this.changeToSlide=function(slideObj) {
            console.log("player.changeTo",slideObj,player);
            this.sendSlideChangingMessage(currentSlide);
            hideSlide(currentSlide);
            this.reloadCurrentSlide();
            currentSlide=$(slideObj);
            self.prepareSlide(currentSlide);
            showSlide(currentSlide);
            this.sendSlideChangedMessage();
        }

        this.showNextSlide = function () {
            try {
                if (slideCount == 0) {
                    console.log("No slides found, cannot proceed with next slide, waiting for reconfiguration");
                    return;
                }

                if (loadingConfig) {
                    console.log("Waiting for config reload to finish!");
                    setTimeout(self.showNextSlide, 500);
                    return;
                }

                $("#im-cp-message").css("display", "none");
                self.removeStaleSlides();
                var olderSlide = null;
                if (!currentSlide) {
                    $(".im-cp-slide").css("visibility", "hidden");
                    $(".im-cp-viewer").css("visibility", "hidden");
                    self.initCurrentSlide();
                    player.sendSlideChangingMessage(currentSlide);
                    player.sendSlideChangedMessage();
                } else {
                    if (slideCount == 1) {
                        showSlide(currentSlide);
                        self.reloadCurrentSlide();
                        setTimeout(function () {
                            self.sendSlideChangedMessage();
                        }, 500);
                        return;
                    }
                    player.sendSlideChangingMessage(currentSlide);

                    var nextSlide = currentSlide.is(".im-cp-slide:last") ? $(".im-cp-slide:first") : currentSlide.next();
                    if (typeof(nextSlide) === 'undefined') nextSlide = $(".im-cp-slide:first");
                    olderSlide = currentSlide;
                    currentSlide = nextSlide;
                    self.prepareSlide(currentSlide);
                    player.sendSlideChangedMessage();

                }
                showSlide(currentSlide);
                if (olderSlide && !slideEquals(olderSlide, currentSlide)) {
                    hideAndUnPrepareSlide(olderSlide);
                    // dirty way to wait for slow repaints, keep current slide visible until the new one gets loaded
    //                    setTimeout(function () {
    //                    }, 200);
                }
                var next = currentSlide.next();
                if (next.length > 0) {
                    // safeguard for excessive iframes (buggy code somewhere else)
                    next.find("iframe:not(:last)").remove();
                } else {
                    next = $(".im-cp-slide:first");
                }
                var n1 = next;
                player.prepareSlide(n1);
                var n2 = next.next();
                if (typeof(n2) !== 'undefined' && n2[0]) {
                    player.prepareSlide(n2);
                }

                console.log("Slide changed to", currentSlide, currentSlide.find("iframe").attr("imcontentsrc"));
            } catch (err) {
                console.log("BUG: showNextSlide: exception caught:", err, " reloading tab");
                window.location.reload();
            }
        };
        this.sendSlideChangingMessage = function (s) {
            self.sendPlayerControlMessage("EndContentPlayback", {duration: s.attr("imduration"), URL: s.find("iframe").attr("imcontentsrc")});
            if (slideCount > 1)
                setTimeout(function () {
                    player.hibernateSlide(s);
                }, 500);
        };
        this.removeStaleSlides = function () {
            var slide;
            while (slide = staleSlides.pop()) {
                $(slide).remove();
            }
        };

        this.sendSlideChangedMessage = function () {
            if (loadingConfig) {
                console.log("Waiting for config reload to finish!");
                setTimeout(self.sendSlideChangedMessage, 500);
                return;
            }
            if (lastSlideChangeTimestamp == null) lastSlideChangeTimestamp = new Date();
            var d = new Date();
            var diff = d.getTime() - lastSlideChangeTimestamp.getTime();
            console.log("Elapsed time after last slide change:", diff, " ms");
            lastSlideChangeTimestamp = d;
            $("#im-cp-message").empty().css("display", "none");
            if (!currentSlide || typeof(currentSlide) == 'undefined') {
                console.log("BUG: we should always have a valid reference to currentSlide, jumping to first:", currentSlide);
                var first = $(".im-cp-slide:first");
                if (first.length == 0) {
                    console.log("No slides found, skipping slide change event and waiting for config update");
                    return;
                }
                currentSlide = first;
            }
            self.sendPlayerControlMessage("StartContentPlayback", {duration: currentSlide.attr("imduration"), URL: currentSlide.find("iframe").attr("imcontentsrc")});
        };



        this.sendPlayerControlMessage = function (name, details) {
            this.callBackendFunction("player.onPlayerControlMessage", {name: name, details: details});
        };
        this.onPlaybackComplete = function () {
            this.showNextSlide();
        };
        this.getCurrentItemPlaybackDuration = function () {
            if (currentSlide != null) {
                return currentSlide.duration;
            }
            return defaultDuration;
        };

        this.getCurrentSlide = function () {
            return currentSlide;
        };
        this.onPlaybackPause = function () {
            pauseISpring();
        };
        this.onPlaybackStart = function () {
            playISpring();
        };
        var pauseISpring = function () {

        };
        var playISpring = function () {

        }
    }


    var IM_timeTableCheckLastRun=0;

    /**
     * @return {boolean}
     */
    function IM_timeTableCheckedForThisMoment(lastrun, currentTime) {
        if (!lastrun) return false;
        return ( lastrun.getSeconds() == currentTime.getSeconds() &&
            lastrun.getMinutes() == currentTime.getMinutes() &&
            lastrun.getHours() == currentTime.getHours()
            );
    }

    function IM_checkTimeTableAndRotate() {
        var c = new Date();
        var slides=$(".im-cp-slide");
        if(IM_timeTableCheckedForThisMoment(IM_timeTableCheckLastRun, c)){
            return;
        }

        for(var i=0;i<slides.length;i++) {
            var slide=slides.get(i);
            var ttSec = slide.getAttribute("imtimetableseconds");
            var ttMin = slide.getAttribute("imtimetableminutes");
            var ttHr =  slide.getAttribute("imtimetablehours");
            if(typeof(ttSec)==='undefined' && typeof(ttMin)==='undefined' && typeof(ttHr)==='undefined') {
                console.log("all null for",slide);
                continue;
            }
            var tsMatch = false;
            var matchType = -1;

            if (typeof(ttSec)!="undefined" && ttSec!=null && ttSec.length > 0 ) {
                tsMatch = ($.inArray(""+c.getSeconds(),ttSec.split(",")) > -1);
                matchType++;
            }
            if (!tsMatch && typeof(ttMin)!="undefined" && ttMin!=null && ttMin.length > 0 ) {
                tsMatch = ($.inArray(""+c.getMinutes(), ttMin.split(",")) > -1);
                matchType++;
            }

            if (!tsMatch && typeof(ttHr)!="undefined" && ttHr!=null && ttHr.length > 0 ) {
                tsMatch = ($.inArray(""+c.getHours(), ttHr.split(",")) > -1);
                matchType++;
            }
            if (tsMatch && (typeof(player.getCurrentSlide())==='undefined' || !player._slideEquals(player.getCurrentSlide(),$(slide)))) {
                console.log("Got timestamp match for:",player.slide, matchType, ttSec, ttMin, ttHr);
                player.changeToSlide(slide);
                IM_timeTableCheckLastRun = c;

                break;
            }
        }


    }
    function IM_ch_initPlayer() {
        if (!inited) {
            windowWidth = window.innerWidth;
            windowHeight = window.innerHeight;
            inited = true;
            player = new IMChromePlayerFrontend();
            /*var firstIdx=-1;
             if(document.location.hash) {
             firstIdx=document.location.hash.substring(1);
             player.setStartupSlide(firstIdx);
             history.pushState('', document.title, window.location.pathname);
             } */

            player.start();
        }
    }
    IM_ch_initPlayer();

    function IM_control_skipToNextSlide() {
        $(document).trigger("imPlaybackComplete", {});
    }
    function IM_control_skipToPreviousSlide() {
        history.go(-1);
    }
    function IM_control_pausePresentation() {
        $(document).trigger("imPlaybackPause", {});
    }

    $(window).keyup(function (evt) {
        switch (evt.which) {
            case 33: // page up
                IM_control_skipToPreviousSlide();
                break;
            case 34: // page down
                IM_control_skipToNextSlide();
                break;
            case 39: // right arrow
                IM_control_skipToNextSlide();
                break;
            case 37: // left arrow
                IM_control_skipToPreviousSlide();
                break;
            case 32: // space

                IM_control_pausePresentation();
                break;
            default:
            // do nothing
        }
        evt.preventDefault();
    });

    //function resizedURL(url,windowWidth,windowHeight) {
    //        url=url.replace(/w=[0-9]*/,"w="+windowWidth);
    //        url=url.replace(/h=[0-9]*/,"h="+windowHeight);
    //       return url;
    //}

    window.addEventListener('resize', function () {
        if (player && (window.innerWidth != windowWidth || window.innerHeight != windowHeight)) {
            windowHeight = window.innerHeight;
            windowWidth = window.innerWidth;
            $("li > iframe").each(function () {
                var f = $(this);
                f.css("width", windowWidth + "px");
                f.css("height", windowHeight + "px");
                var url = f.attr("imcontentsrc");
                if (f.attr("src")) {
                    f.attr("src", player.prepareURL(url), f.parent().attr("imduration"));
                }
            });
            setTimeout(function () {
                player.sendSlideChangedMessage();
            }, 3000);

        }
    });


    window.addEventListener('error', function (evt) {
        console.log("###################### ERROR: Caught[via 'error' event]:  '" + evt.message + "' from " + evt.filename);
        console.log(evt);
        if (evt.filename && evt.filename.indexOf('chrome-extension:') == 0) {
            setTimeout(window.location.reload, 5000);
        }
        evt.preventDefault();
    });
