var dialogCheckerCount=0;
var dialogChecker;

function chromeDialogChecker() {
chrome.extension.sendMessage({name: "GetOriginalWindowInnerHeight", url: window.location.href, height:window.innerHeight}, function(response) {
        if(response && response.height) {
            if(response.height>0 && window.innerHeight<response.height) {
                if(dialogCheckerCount==2) {
                   // if(pluginChecker) clearInterval(pluginChecker);
                    if(dialogChecker) clearInterval(dialogChecker);
                    dialogCheckerCount=0;
                    console.log("window innerheight decrease detected, emitting PluginCrashed/Reload",window,response.height,window.innerHeight);
                    chrome.extension.sendMessage({name: "PluginCrashed"}, function(response) {
                    });

                } else {
                    dialogCheckerCount++;
                    console.log("Detected window innerheight decrease, rechecking");
                }
            } else {
            }
        } else {
            console.log("chromeDialogChecker: response not defined", response);

        }
    }
);
}
chrome.extension.sendMessage({name: "UpdateWindowInnerHeight", url: window.location.href, height:window.innerHeight}, function(response) {
    chromeDialogChecker();
});
$(window).resize(function () {
   chromeDialogChecker();
});
