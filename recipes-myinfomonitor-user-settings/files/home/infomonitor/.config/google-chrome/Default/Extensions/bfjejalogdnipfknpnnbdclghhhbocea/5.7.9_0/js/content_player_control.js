/**
 *
 * Copyright (c) 2012 Cemron Ltd
 *
 **/
var preloaderEnabled = false;
var pluginCrashed = "var evt=document.createEvent('Events');evt.initEvent('cim-plugincrashed',true,true);window.dispatchEvent(evt);";
var playerRequestAck = "var evt=document.createEvent('Events');evt.initEvent('cim-playercontrolack',true,true);window.dispatchEvent(evt);";
var beforeSlideActivation = "var evt=document.createEvent('Events');evt.initEvent('cim-beforeslideactivation',true,true);window.dispatchEvent(evt);";
var slideActivation = "var evt=document.createEvent('Events');evt.initEvent('cim-slideactivation',true,true);window.dispatchEvent(evt);";


function cleanUpSlideURL(url) {
    url=url.replace(/[?&]w=[0-9]*/,'');
    url=url.replace(/[?&]h=[0-9]*/,'');
    url=url.replace(/[?&]d=[0-9]*/,'');
    url=url.replace(/[?&]__im_offline_cache_id=[a-zA-Z0-9]*/,'');
    return url;

}
chrome.extension.sendMessage({name: "PreloaderEnabled", url: window.location.href}, function(response) {
            preloaderEnabled=response;
    }
);

chrome.extension.sendMessage({name: "Settings"}, function(settings) {
    if (settings && settings != ''){
         if (settings["IMRotator"] == 'CRRotator') {
                console.log("Keypresses get detected, tab control active!");
                $(window).keydown(function(evt) {
                    switch (evt.which) {
                        case 33: // page up
                            IM_control_skipToPreviousSlide();
                            break;
                        case 34: // page down
                            IM_control_skipToNextSlide();
                            break;
                        case 39: // right arrow
                            IM_control_skipToNextSlide();
                            break;
                        case 37: // left arrow
                            IM_control_skipToPreviousSlide();
                            break;
                        case 32: // space
                            IM_control_pausePresentation();
                            break;
                        default:
                        // do nothing
                    }
                  //  evt.preventDefault();
                });
          }
    }
});

function IM_control_skipToNextSlide() {
    chrome.extension.sendMessage({name: "ShowNextTab"}, function(response) {


    });
}

function IM_control_skipToPreviousSlide() {
    chrome.extension.sendMessage({name: "ShowPreviousTab"}, function(response) {});
}

function IM_control_pausePresentation() {
    chrome.extension.sendMessage({name: "ToggleRotation"}, function(response) {


    });
}

var lastPreloadURI=null;
var messageMutex=false;

function contentMessageHandler(event) {
    if(messageMutex) {
        setTimeout(function(){
            contentMessageHandler(event);
        },50);
    }
    messageMutex=true;
    try {
        if(!event.data || typeof(event.data.name)==='undefined' || event.data.name!=='imPlayerControl') {
            //console.log("imPlayerControl (Extension): this event is not for us",event);
            return;
        }
        var actualMsg=event.data.value;

        switch(actualMsg) {
            case "capabilities":
                console.log("imPlayerControl (Extension): TODO Implement capabilities query");
                //preloaderEnabled?actualMsg="capabilities:preloader":actualMsg="capabilities:simpleloading";
                break;
            case "imPlaybackComplete":
            case "rotate":
                IM_control_triggerSlideChange();
                break;
            case "lastpage":
                IM_control_emitLastPageReached();
                break;
            default:
                var commandDelim=actualMsg.indexOf(':');
                if(commandDelim==-1) {
                    console.log("Invalid message", actualMsg);
                    return;
                }
                var command=actualMsg.substring(0,commandDelim);
                var data=actualMsg.substring(commandDelim+1);
                switch(command) {
                    case 'preload':
                        if(!lastPreloadURI || data != lastPreloadURI) {
                            console.log("preloading:", data);
                            IM_control_preloadSlide(data);
                            lastPreloadURI=data;
                        } else if(data==lastPreloadURI){
                            console.log("double fire of preload event:", data, lastPreloadURI);
                        }
                        break;
                    case 'viewtimechanged':
                    case 'imPlaybackDurationChange':
                        IM_control_handleViewTimeChanged(data);
                        break;
                    case 'imSubSlidePlaybackComplete':
                        IM_control_handleSubFrameSlidePlaybackComplete(data);
                        break;
                    default:
                        console.log("Invalid message (message not recognized:",actualMsg);
                }
        }
    } catch(e) {
        console.log("Exception in content message handling",e);
    }
    messageMutex=false;
}

window.addEventListener('message',contentMessageHandler,false);

chrome.extension.onMessage.addListener(function(data, from, responseCallback) {
    if((from && from.tab && from.tab.id>-1) || typeof(data.details)==='undefined') {
        //console.log("content_player_control, skipped message",data);
        return;
    }

    var myURL=cleanUpSlideURL(document.location.href);
    var targetURL=typeof(data.details.URL)!=='undefined'?cleanUpSlideURL(data.details.URL):"";
    if(myURL!=targetURL) {
        //console.log("Background message is not for us: url", myURL," target:", targetURL,data);
        return;
    }
    var msgSent=false;
    switch (data.name) {
        case 'EndContentPlayback':
            console.log("Sending Content Playback End Command to slide", data.details.URL, document.location.href);
            window.postMessage("cim-slide-deactivation","*");
            msgSent=true;
            break;

        case 'ReloadFrame':
        case 'ReloadCurrentSlide':
            console.log("Reloading",window.location.href);
            // send message beforehand
            chrome.extension.sendMessage({name: "PlayerControlMessageACK", msgid:data.details.msgid}, function(response) {

            });
            // do reload
            window.location.reload();
            return;
            break;
        case 'StartContentPlayback':
            console.log("Sending Content Playback Start Command to slide", data.details.URL, document.location.href);
            window.postMessage("cim-slideactivation","*");
            // let's make sure that the page gets the message
            $(document).ready(function() {
                window.postMessage("cim-slideactivation","*");
            });
            IM_system_pingWatchdog(data.details.duration);
            msgSent=true;
            break;

    }
    if(msgSent){
        //console.log("Sending ACK:",data.details.msgid,data.name, data);
        chrome.extension.sendMessage({name: "PlayerControlMessageACK", msgid:data.details.msgid}, function(response) {

        });
    }

});


function IM_control_emitACK() {
    console.log("sending ACK");
    chrome.extension.sendMessage({name: "PlayerRequestACK"}, function(response) {

    });
}
function IM_control_emitLastPageReached () {
    chrome.extension.sendMessage({name: "LastPageReached"}, function(response) {

    });
}
function IM_control_preloadSlide(url) {
    chrome.extension.sendMessage({name: "PreloadSlide", url:url}, function(response) {

    });

}
function IM_control_triggerSlideChange(){
    chrome.extension.sendMessage({name: "ChangeSlide", url:document.location.href}, function(response) {
    });

}
/*$(document).ready(function(){
    console.log("document",document.location.href,$('div[imframesrc]').length);
    injectJS("$(document).ready(function(){console.log('divs',$('div[imframesrc]'))});","jep");
});*/
function IM_control_handleSubFrameSlidePlaybackComplete(sourceFrame){
    chrome.extension.sendMessage({name: "PlaybackDurationChange", details:{duration:22222, url:document.location}}, function(response) {


    });
    var frameContainers=$('div[imframesrc]');
    if(sourceFrame.indexOf('?')>-1) sourceFrame=sourceFrame.substring(0,sourceFrame.indexOf('?'));
    for(var i=0;i<frameContainers.length;i++) {
        var fc=$(frameContainers[i]);
        var fSrc=fc.attr('imframesrc');
        if(fSrc.indexOf('?')>-1) fSrc=fSrc.substring(0,fSrc.indexOf('?'));
        if((fSrc.indexOf('/')==0 && sourceFrame.indexOf(fSrc)>-1) || fc.attr('imframesrc')===sourceFrame) {
            console.log("circumventing Chromium bug https://code.google.com/p/chromium/issues/detail?id=1373");
            fc.children().css("visibility","hidden");
            fc.unbind('load.playerfix');
            fc.children().bind('load.playerfix',function(){
                setTimeout(function() {
                    $(this).css("visibility","visible");
                },20);
            });
        }
    }


}


function IM_control_handleViewTimeChanged(t) {
    console.log("Got view time change request",t, document.location);
    var time=Math.round(t);
    if(time==1) return;
    chrome.extension.sendMessage({name: "PlaybackDurationChange", details:{duration:time, url:document.location}}, function(response) {
    });

}

function IM_system_pingWatchdog(t){
    var time=Math.round(t);
    if(time==1) return;
    chrome.extension.sendMessage({name: "ResetWatchdog", newTimeout:time}, function(response) {
    });
}









