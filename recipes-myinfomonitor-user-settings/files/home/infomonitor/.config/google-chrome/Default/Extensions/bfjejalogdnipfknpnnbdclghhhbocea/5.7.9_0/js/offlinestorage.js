function OfflineStorage(callback) {
    var self=this;
    var fetching=false;
    var fetchMaxWaitCount=100;
    var fetchTrialCount=0;
    this.storageDirEntry=null;
    this.storageFS=null;
    var currentlyFetchedURL="";
    var currentlyFetchedItem=null;
    var queuedURLs=[];
    var binaryChunkSize=5000000;
    var fetchPauseMillis=2000;
    var dbName="offlinedatav2";
    var dbObjectName="offline";

    var dbVersion=4.2;
    this.currentOfflineURLs={};


    window.indexedDB = window.indexedDB || window.webkitIndexedDB || window.mozIndexedDB || window.msIndexedDB;
    var osDB = {};
    osDB.indexedDB = {};
    osDB.READ_ONLY="readonly";
    osDB.READ_WRITE="readwrite";

    osDB.indexedDB.onerror = function(e) {
      console.log("IndexedDB Error",e);
    };

    osDB.indexedDB.db=null;

    osDB.indexedDB.open = function(callback) {
        var request = indexedDB.open(dbName,dbVersion);
        request.onupgradeneeded = function(e) {
          var db = e.target.result;
          console.log("Upgrade needed",db);
          e.target.transaction.onerror = osDB.indexedDB.onerror;
          if(db.objectStoreNames.contains(dbObjectName)) {
            db.deleteObjectStore(dbObjectName);
          }
         var objectStore = db.createObjectStore(dbObjectName, { keyPath: "filename",autoIncrement:false });
         objectStore.createIndex("onlineURL", "onlineURL", { unique: false });

        };
        request.onsuccess = function(e) {
            osDB.indexedDB.db = e.target.result;
            console.log("DB opened: ", e, e.target.result);
            if(callback) callback();
        };
        request.onerror = osDB.indexedDB.onerror;

    };

    var fetcherXHR=null;

    function cleanUpURL(url) {
        if(typeof(url)==='undefined' || !url) return url;
        url=url.replace(/[?&]w=[0-9]*/,'');
        url=url.replace(/[?&]h=[0-9]*/,'');
        url=url.replace(/[?&]d=[0-9]*/,'');
        url=url.replace(/[?&]i=[0-9]*/,'');
        // nocache filter
        url=url.replace(/[?&]nocache=[0-9]*/,'');
        // json and jsonp filters
        url=url.replace(/[?&]_=[a-zA-Z0-9_-]*/,'');
        url=url.replace(/[?&][_]?jsonp=[a-zA-Z0-9_-]*/,'');
        url=url.replace(/[?&][_]?json[cC]allback=[a-zA-Z0-9_-]*/,'');
        url=url.replace(/[?&][_]?callback=[a-zA-Z0-9_-]*/,'');
        // extjs cache filter
        url=url.replace(/[?&]_dc=[a-zA-Z0-9_-]*/,'');

        return url;

    }

    var fetcherOnErrorHandler = function (e) {
        setTimeout(fetchURL, fetchPauseMillis);
        osDB.saveURL(currentlyFetchedItem.url, [], currentlyFetchedItem.responseHeaders);
    };
    var fetcherOnLoadHandler = function (e) {
        setTimeout(fetchURL, fetchPauseMillis);

        if (this.status == 200 || this.status == 206) {
            console.log("OfflineStorage: URL fetched successfully:", currentlyFetchedItem.url, e, this.response.size);
            oStorage.saveURL(currentlyFetchedItem.url, this.response, currentlyFetchedItem.responseHeaders, currentlyFetchedItem.range, currentlyFetchedItem.contentLength);
            fetching = false;
        }
        if (this.status == 404) {
            oStorage.saveURL(currentlyFetchedItem.url, [], currentlyFetchedItem.responseHeaders);
            fetching = false;
        }
    };
    var getMimetypeString = function (rawMimetype) {
        if (typeof(rawMimetype) === 'undefined' || rawMimetype === '') return "application/x-shockwave-flash";
        if (rawMimetype.indexOf(";") > -1) return rawMimetype.substring(0, rawMimetype.indexOf(';'));

        return rawMimetype;
    };
    var fetchURL = function () {
        if (fetching) {
            console.log("Waiting for earlier fetching to complete..., earlier url:", currentlyFetchedURL);
            setTimeout(fetchURL, 1000);
            return;
        }
        fetching = true;
        fetchTrialCount = 0;
        currentlyFetchedItem = queuedURLs.pop();
        if (typeof(currentlyFetchedItem) !== 'undefined' && typeof(currentlyFetchedItem) == 'object') {
            currentlyFetchedURL = currentlyFetchedItem.url;
            if (fetcherXHR) delete fetcherXHR;
            fetcherXHR = new XMLHttpRequest();
            fetcherXHR.open('GET', currentlyFetchedItem.url, true);
            if (typeof(currentlyFetchedItem.range) == 'object') {
                fetcherXHR.setRequestHeader("Range", "bytes=" + currentlyFetchedItem.range.start + "-" + currentlyFetchedItem.range.end);
            }
            fetcherXHR.responseType = 'blob';
            fetcherXHR.onerror = fetcherOnErrorHandler;
            fetcherXHR.onload = fetcherOnLoadHandler;
            setTimeout(function () {
                if (!currentlyFetchedItem.range || typeof(currentlyFetchedItem.contentLength) === 'undefined') {
                    console.log("RANGE NOT SET, SKIPPING", currentlyFetchedItem.url, currentlyFetchedItem);
                    setTimeout(fetchURL, fetchPauseMillis);
                } else {
                    fetcherXHR.send(null);
                }
            }, 1000);
        } else {
            fetching = false;
            setTimeout(fetchURL, fetchPauseMillis);
        }


    };

    setTimeout(fetchURL,fetchPauseMillis);

    this.getRedirectURL = function (onlineURL) {
        var r = cleanUpURL(onlineURL);
        r.indexOf('?') == -1 ? r += "?" : r += "&";
        r += "__im_offline_cache_id=" + chrome.i18n.getMessage("@@extension_id");
        return r;
    };

    this.getHeaderValue = function (headers, header) {
        if (!headers) return;
        for (var i = 0; i < headers.length; i++) {
            var v = headers[i];
            if (v.name.toLowerCase() == header) {
                return v.value;
            }
        }

    };
    this.updateCacheMetadata = function (url, headers, filename, offlineUrl) {
        var fn = filename;
        if (!filename) fn = this.filename(url);

        var itemObject = {
            filename: fn,
            onlineURL: cleanUpURL(url),
            offlineURL: offlineUrl,
            lastModified: self.getHeaderValue(headers, "last-modified"),
            eTag: self.getHeaderValue(headers, "etag"),
            expires: self.getHeaderValue(headers, "expires"),
            contentType: getMimetypeString(self.getHeaderValue(headers, "content-type")),
            lastAccess: new Date()
        };
        console.log("Updating metadata:", itemObject);
        var db = osDB.indexedDB.db;
        var trans = db.transaction([dbObjectName], osDB.READ_WRITE);
        var store = trans.objectStore(dbObjectName);
        var request = store.put(itemObject);

        request.onerror = function (e) {
            console.log("Offline storage update failed", e, itemObject);
        }

    };

    this.originalFilename = function (url) {
        var a = document.createElement('a');
        a.href = url;
        return a.pathname.split('/').pop();
    };
    this.filename = function (url) {
        var r = cleanUpURL(url);
        if (r.indexOf("__im_offline_cache_id") == -1) {
            r.indexOf('?') == -1 ? r += "?" : r += "&";
            r += "__im_offline_cache_id=" + chrome.i18n.getMessage("@@extension_id");
        }
        return btoa(r).replace('/', ',') + this.originalFilename(url);
    };
    this.url = function (filename) {
        var v = filename.substring(filename.length - 4);
        return atob(v.replace(',', '/'));
    };
    this.addToOfflineList = function (filename, mimetype, offlineurl) {
        if (!offlineurl) {
            console.log("Fetching offline url from local filesystem, impaired performance.");
            self.storageFS.root.getDirectory('InfomonitorOfflineStorage', {create: true}, function (storageDirEntry) {
                storageDirEntry.getFile(filename, {create: false}, function (fEntry) {
                    self.currentOfflineURLs[filename] = fEntry.toURL(getMimetypeString(mimetype));
                });
            });
        } else {
            self.currentOfflineURLs[filename] = offlineurl;
        }
    };
    this.removeFromOfflineList = function (filename) {
        var c = self.currentOfflineURLs[filename];
        if (typeof(c) == 'string') {
            delete self.currentOfflineURLs[filename];
        }
    };
    this.saveURL = function (onlineURL, binarydata, httpHeaders, range, contentLength, errorHandler) {
        if (!errorHandler) errorHandler = fsErrorHandler;
        var b = self.filename(onlineURL);
        // Content-Type is not used atm, should be - but needs to be implemented correctly when loading
        // old files from storage
        var contentType = getMimetypeString(self.getHeaderValue(httpHeaders, "content-type"));
        var rangeAndLengthSet = (typeof(range) !== 'undefined' && typeof(contentLength) !== 'undefined');
        self.storageFS.root.getDirectory('InfomonitorOfflineStorage', {create: true}, function (storageDirEntry) {
            storageDirEntry.getFile(b, {create: true, exclusive: false}, function (fileEntry) {
                fileEntry.getMetadata(function (metadata) {
                    fileEntry.createWriter(function (fileWriter) {

                        var fileOfflineURL = fileEntry.toURL(contentType);
                        var nonRangeWriteEnd = function (evt) {
                            self.updateCacheMetadata(onlineURL, httpHeaders, b, fileOfflineURL);
                            self.addToOfflineList(b, contentType, fileOfflineURL);
                        };
                        var rangeWriteComplete = function (evt) {
                            //console.log("Range write complete",evt);
                            self.addToOfflineList(b, contentType, fileOfflineURL);
                            self.updateCacheMetadata(onlineURL, httpHeaders, b, fileOfflineURL);
                        };
                        var truncateComplete = function (evt) {
                            //console.log("File truncated to content length,  seeking to",range.start);
                            fileWriter.onwriteend = rangeWriteComplete;
                            fileWriter.seek(range.start);
                            var blob = new Blob([binarydata], {type: contentType});
                            fileWriter.write(blob);
                        };

                        fileWriter.onerror = function (evt) {
                            console.log('Write failed: ' + e);
                        };

                        if (rangeAndLengthSet) {
                            fileWriter.onwriteend = rangeWriteComplete;
                            if (metadata.size == 0) {
                                fileWriter.onwriteend = truncateComplete;
                                fileWriter.truncate(contentLength);
                            } else {
                                fileWriter.seek(range.start);
                                var blob = new Blob([binarydata], {type: contentType});
                                fileWriter.write(blob);
                            }

                        } else {
                            fileWriter.onwriteend = nonRangeWriteEnd;
                            var blob = new Blob([binarydata], {type: contentType});
                            fileWriter.write(blob);
                        }

                    }, errorHandler);
                });
            }, errorHandler);

        });
    };

    var addToQueue = function (url, responseHeaders) {
        if (typeof(url) === 'undefined' || url === '' || typeof(responseHeaders) === 'undefined') return;
        if (typeof(currentlyFetchedItem) !== 'undefined' && currentlyFetchedItem != null) {

            if (currentlyFetchedItem.url === url) {
                console.log("Cyclic add to queue, trashing request");
                return;
            }
        }
        for (var i = 0; i < queuedURLs.length; i++) {
            var q = queuedURLs[i];
            if (typeof(q) === 'object' && q.url === url) return;
        }
        var cLen = self.getHeaderValue(responseHeaders, "content-length");
        if (cLen < 1) {
            //queuedURLs.push({url: url, responseHeaders: responseHeaders});
            console.log("WARNING: Not offline caching content without content-length header (by design)");
        } else {
            if (typeof(cLen) === 'undefined' || cLen == null) {
                console.log("UNDEFINED CONTENT-LENGTH");
                cLen = binaryChunkSize;
            }
            var rangeStart = 0;
            var rangeEnd = 0;
            do {
                rangeStart = rangeEnd == 0 ? 0 : rangeEnd + 1;
                rangeEnd = rangeStart + binaryChunkSize;
                if (rangeEnd > cLen - 1)rangeEnd = cLen - 1;
                queuedURLs.push({url: url, responseHeaders: responseHeaders, range: {start: rangeStart, end: rangeEnd}, contentLength: cLen});
            } while (rangeEnd < cLen - 1);
            console.log("Queued url for fetching:", url);
        }
    };

    this.queueURLForFetching = function (url, responseHeaders) {
        addToQueue(url, responseHeaders);
    };
    this.getURLSync = function (onlineURL) {
        var b = self.filename(onlineURL);
        return self.currentOfflineURLs[b];
    };
    this.getURL = function (onlineURL, callback, errorHandler) {
        if (!errorHandler) errorHandler = fsErrorHandler;
        var b = self.filename(onlineURL);
        self.storageFS.root.getDirectory('InfomonitorOfflineStorage', {create: true}, function (storageDirEntry) {
            storageDirEntry.getFile(b, {create: false}, function (fEntry) {
                callback(fEntry.toURL());
            }, errorHandler);
        });
    };

    // url: item web url
    // httpHeaders: response http headers
    // callback: function(url,expired) expired = bool, true:is expired, false:is fresh
    this.getStatus = function (onlineURL, callback) {
        self.getURL(onlineURL, function (fsURL) {
            var db = osDB.indexedDB.db;
            var trans = db.transaction([dbObjectName], osDB.READ_ONLY);
            var store = trans.objectStore(dbObjectName);
            var urlIdx = store.index("onlineURL");
            var request = urlIdx.get(cleanUpURL(onlineURL));
            request.onsuccess = function (e) {
                var expired = true;
                if (e.target.result) expired = false;
                callback(onlineURL, expired);
            };
            request.onerror = function (e) {
                var db = osDB.indexedDB.db;
                console.log("OFFLINE STORAGE: Expired check query failed", e, onlineURL);
                callback(onlineURL, true);
            };
        }, function (e) {
            callback(onlineURL, true);
        });
    };
    this.truncateStorage = function (callback) {
        console.log("Truncating offline storage");
        removeOfflineDir(function () {
            var setVrequest = osDB.indexedDB.db.setVersion(dbVersion);
            setVrequest.onsuccess = function (e) {
                osDB.indexedDB.db.deleteObjectStore(dbName);
                createOfflineBackend();
                if (callback) callback();
            };
            setVrequest.onerror = function (e) {
                console.log("Offline storage indexedDB remove failed, trying to recreate", e);
                createOfflineBackend();
                if (callback) callback();
            }
        })

    };

    var removeOfflineDir = function (callback, errorCallback) {
        self.storageFS.root.getDirectory('InfomonitorOfflineStorage', {create: false}, function(dirEntry) {
            dirEntry.removeRecursively(callback, errorCallback);
        });

    };
    var initializeDB = function (callback) {
        osDB.indexedDB.open(function () {
            var db = osDB.indexedDB.db;
            var trans = db.transaction([dbObjectName], osDB.READ_ONLY);
            var store = trans.objectStore(dbObjectName);
            var cursorRequest = store.openCursor();
            cursorRequest.onsuccess = function (evt) {
                var cursor = evt.target.result;
                if (!cursor) {
                    if (callback) callback();
                } else {
                    var obj = cursor.value;
                    if (typeof(obj.filename) == 'string') {
                        self.addToOfflineList(obj["filename"], getMimetypeString(obj["contentType"]), obj["offlineURL"]);
                        //console.log("Offline file registered:",obj, obj["filename"]," with mimetype:",getMimetypeString(obj["contentType"]));
                    }
                    cursor.continue();

                }
            };
            cursorRequest.onerror = function (e) {
                console.log("Offline metadata not found, offline file list not initialized", e);
                if (callback) callback();
            }

        });
    };


    var createOfflineBackend = function (callback) {
        self.storageFS.root.getDirectory('InfomonitorOfflineStorage', {create: true}, function (dirEntry) {
            self.storageDirEntry = dirEntry;
            console.log("Directory created", dirEntry.toURL(), dirEntry);
            initializeDB(callback);

        }, this.fsErrorHandler);
    };
    var onInitFS = function (fs, callback) {
        self.storageFS = fs;
        createOfflineBackend(callback);
    };

    var fsErrorHandler = function (e) {
        var msg = '';

        switch (e.code) {
            case FileError.QUOTA_EXCEEDED_ERR:
                msg = 'QUOTA_EXCEEDED_ERR';
                break;
            case FileError.NOT_FOUND_ERR:
                msg = 'NOT_FOUND_ERR';
                break;
            case FileError.SECURITY_ERR:
                msg = 'SECURITY_ERR';
                break;
            case FileError.INVALID_MODIFICATION_ERR:
                msg = 'INVALID_MODIFICATION_ERR';
                break;
            case FileError.INVALID_STATE_ERR:
                msg = 'INVALID_STATE_ERR';
                break;
            default:
                msg = 'Unknown Error';
                break;
        }
        console.log('Offline storage error: ' + msg, e);
    };


    window.webkitStorageInfo.requestQuota(window.PERSISTENT, 10000*1024*1024, function(grantedBytes) {
        window.webkitRequestFileSystem(window.PERSISTENT, grantedBytes, function(fs) {
            onInitFS(fs,callback);

        }, fsErrorHandler);
    }, function(e) {
        console.log('Offline storage init failed', e);
    });




}