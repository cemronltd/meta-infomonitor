// JavaScript Document

/*
Plugin: Sideswap v 1.0.1
Author: BrianBlocker.com
Tools: Dreamweaver CS4 / 2009 iMac (early edition) / 2009 Mac Mini (late edition)
*/

(function($)
{					
	$.fn.sideswap = function(options)
	{
		$.fn.sideswap.defaults = 
		{
			previous: 'prev', /* HTML TO BE USED FOR THE PREVIOUS ARROW */
			next: 'next', /* HTML TO BE USED FOR THE NEXT ARROW */
			display_time: 5000, /* NUMBER OF 1/1000 SECONDS AN item IS DISPLAYED */
			transition_speed: 200, /* SPEED OF TRANSITIONS IN 1/1000 SECONDS */
			mode: 'default', 	/* default or ticker */
			ticker_mode: 'reveal_from_bottom',
			auto_run: true, /* AUTOMATICALLY TRANSITIONS THE SLIDES (IF TRUE) */
			loop_times: 1 /* LIST IS LOOPED n TIMES */
		};
				
		var opts = $.extend({}, $.fn.sideswap.defaults, options), /* PUTS ALL THE DEFAULT VALUES INTO A VARIABLE */
				clicked = false; /* TRUE AFTER A PREVIOUS OR NEXT BUTTON HAS BEEN CLICKED, HELPS PREVENT ERRORS */
		
		var $firstItem=null;
		
		alert('sideswap autostart='+autostart);
		
		if(autostart == 'event'){

        } else {
            return doSS(this);
        }

		function doSS(obj) {
		return obj.each(function()
		{
			/* VARIABLES */
			var $this = $(this), /* SETS A "GLOBAL" VARIABLE FOR THE CURRENT MATCHED OBJECT */
					$parent = $this.parent(), /* SETS A "GLOBAL" VARIABLE FOR THE MATCHED OBJECT'S PARENT */
					timer = false, /* THE SETINTERVAL FUNCTION IDENTIFIER */
					$items = $this.children(), /* THE items TO BE ROTATED */
					count = $items.size(); /* THE TOTAL NUMBER OF items THAT WILL BE ROTATED */
			
			/* SETUP THE items */
			
			setup();			
			
			/* SETS UP THE item DISPLAYS */
			function setup()
			{
				/* OVERRIDE THE CSS STYLES */
				$this.css('position','relative');
			
				$items.css({"position":"absolute","top":"0px","left":"0px"});
							
				if(opts.mode && opts.mode == 'ticker') {
					$this.css("white-space", "nowrap");
					$items.css({"width":"100%","text-align":"center"});
					var pfs=56;
					$items.each(function(i,el) {
						var npfs=$(el).projectedFontSize({minFontSize:12});
						if(npfs<pfs)pfs=npfs;
					});
					var firstItem=null;
					$items.each(function(i,el) {
						$(el).children().css("font-size",pfs+"px");
						switch(opts.ticker_mode){
							case 'reveal_from_bottom':
								$(el).css("top","100px");
								$(el).css("visibility","hidden");
								break;
							case 'horizontal':
								$(el).css("left",$this.width()+"px");
								$(el).css("visibility","hidden");
								break;
						}
						if(i==0) $firstItem=$(el);
					});
					//console.log($this.children());
					
				} else {
					$items.removeClass('hide').css("visibility","hidden");
				}
				

					
				
				/* SET THE ROTATOR TO RUN AUTOMATICALLY IF AUTO IS TRUE, AND MORE THAN 1 OBJECT EXISTS TO ROTATE */
				if(opts.auto_run)
				{
					initiateInterval();
					
					/* PAUSE THE ROTATOR WHEN THE USER HAS THEIR MOUSE OVER AN item */
					/*$parent.hover(function()
					{
						clearInterval(timer);
					},function()
					{
						initiateInterval();
					});*/
				}
			}
			
			/* CONSTRUCTOR */
			function continueIM() {
				if(typeof continueRotation == 'function') {
					clearInterval(timer);
					resetPageChange(opts.display_time);
				}
			}
			function hideItem(item,f) {
				switch(opts.ticker_mode){
					case 'reveal_from_bottom':
						item.css("visibility","hidden");
						item.removeClass("ssvisible");

						break;
					case 'horizontal':
						item.css("visibility","hidden");
						item.removeClass("ssvisible");
						break;
					default:
						item.fadeOut(opts.duration,f);
				}
			}
			function showItemId(itemid) {
				showItem($("#"+itemid),null);
			}
			function showItem(item,f) {
				switch(opts.ticker_mode){
					case 'reveal_from_bottom':
						item.css("visibility","visible");
						item.addClass("ssvisible");
						item.css("-webkit-transform","translateZ(0)");
						/*Firmin.animate(item[0],{
							properties:["top"],
							duration:"2s",
							delay:"1ms",
							top:"0px"
						}, "2s");*/
						item.css("-webkit-animation-name","revealfrombottom");
					   	item.css("-webkit-animation-duration","1s");
					   	item.css("-webkit-animation-timing-function","linear");
						item.css("-webkit-animation-fill-mode","forwards");
						item.show();
							
						break;
					case 'horizontal':
						var durat=(opts.display_time*2)+"ms";
						var displacement="-"+(item.width()+$this.width())+"px";
						item.css("visibility","visible");
						item.addClass("ssvisible");
						Firmin.animate(item[0],{
							translate: {
								x: displacement,
								timingFunction: 'linear'								
							},
							timingFunction: 'linear'

							
						}, durat);	
						break;					
					default:
						item.fadeIn(opts.duration,f);

				}
			}
			function rotate(direction)
			{
				var $previous_item = null;
				var $next_item = null;
				var $current_item = $this.children('.ssvisible:first').size() == 0 ? $this.children(':first') : $this.children('.ssvisible:first');
				if($firstItem) {
					$next_item=$current_item;
					$firstItem=null;
				} else {
					$previous_item = $current_item.prev() == 'undefined' || $current_item.prev().html() == null ? $this.children(':last') : $current_item.prev();
					$next_item = $current_item.next() == 'undefined' || $current_item.next().html() == null ? /*$this.children(':first')*/ continueIM(this) : $current_item.next();
				}
				if(count > 1 && $next_item)
				{
					hideItem($current_item);
					
					switch(direction)
					{
						case -1:
							showItem($previous_item,function()
							{
								clicked = false;
							});
							break;
						default:
							showItem($next_item,function()
							{
								clicked = false;
							});
							break;
					}
				}
			}
			
			
			/* INITIATES THE INTERVAL TIMER */
			function initiateInterval()
			{
				var $firstLoop=true;
				timer = setInterval(function()
				{
					rotate();
				},opts.display_time);
			}
			
		});
        }
	};
})(jQuery);