// SAMPLE
this.i18n = {
    "settings": {
        "en": "Settings",
        "de": "Optionen"
    },
    "productsettings": {
        "en": "InfoMonitor Settings",
        "fi": "InfoMonitor: Asetukset"
    },
    "search": {
        "en": "Search",
        "de": "Suche"
    },
    "nothing-found": {
        "en": "No matches were found.",
        "de": "Keine Übereinstimmungen gefunden."
    },
    "extensionmode": {
        "en": "Mode Selection",
        "fi": "Soitintilan valinta"
    },
    "serverrotator": {
        "en": "Server Mode (Server controlled playback)",
        "fi": "Palvelintila (Palvelin ohjaa esitystä)"
    },
    "chromeplayer": {
        "en": "Player Mode (Player controlled playback)",
        "fi": "Soitintila (Soitinohjelmisto ajaa esityksen itsenäisesti)"
    },
    "playermodebasic": {
        "en": "Basic Settings",
        "fi": "Perusasetukset"
    },
    "basic-player": {
        "en": "Basic Settings",
        "fi": "Perusasetukset"
    },
    "offlinemode": {
        "en": "Offline support enabled",
        "fi": "Offline-toiminto päällä"
    },
    "basic": {
        "en": "Server Mode Settings",
        "fi": "Perusasetukset (palvelintila)"
    },
    "advanced": {
        "en": "Advanced Settings",
        "fi": "Lisäasetukset"
    },
    "ttl": {
        "en": "Presentation TTL",
        "fi": "Esityksen näyttökertojen määrä ennen uudelleenkäynnistystä"
    },

    "css": {
        "en": "Alternate CSS Settings",
        "fi": "Sivustokohtaiset CSS-tiedostot"
    },
    "csstarget": {
        "en": "Target Page URL",
        "fi": "Kohdesivun osoite"
    },
     "csstargetexample": {
        "en": "http://server/page.html",
        "fi": "http://palvelin/sivu, jos annetaan palvelimen juuri, sääntö kohdistuu kaikkiin sivuihin."
    },
    "cssurl": {
        "en": "Alternate CSS File Location (URL)",
        "fi": "Vaihtoehtoisen CSS-tiedoston osoite(URL)"
    },
     "cssurlexample": {
        "en": "http://server/myown.css ",
        "fi": "http://palvelin/myown.css"
    },
    "presentation": {
        "en": "Presentation",
        "fi": "Esitys"
    },
    "rotator": {
        "en": "Render engine mode",
        "fi": "Kalvon latausmoottorin tila"
    },
    "rotatorim": {
        "en": "Preload: preloader enabled, two tabs (zones not supported, instant slide change)",
        "fi": "Esilataus seuraavalle kalvolle (ei zone-tukea, kalvojen välillä ei viivettä)"
    },
    "rotatorim-onetab": {
        "en": "Simple: single tab, no preloader (standard browser mode, zone support)",
        "fi": "Ilman esilatausta (perustila, zone-tuki)"
    },
    "rotatorcr": {
        "en": "CR: Single presentation in separate tabs (zones not supported, only ONE presentation, instant slide change)",
        "fi": "Yksi esitys, esityksen jokainen kalvo omassa tabissaan (ei zone-tukea, vain YKSI esitys, kalvojen välillä ei viivettä)"
    },
    "mode": {
        "en": "Presentation mode",
        "fi": "Esitystila"
    },
    "modesingle": {
        "en": "Single Presentation (NOTE: USE this for CR) ",
        "fi": "Yksittäinen esitys"
    },
    "modescreen": {
        "en": "Screen (set Player UUID or install Certificate)",
        "fi": "Näyttö (syötä näytön UUID tai asenna sertifikaatti)"
    },
    "presentationurl": {
        "en": "Server or presentation URL",
        "fi": "Palvelimen tai esityksen URL"
    },
    "playerid": {
        "en": "Player UUID (number)",
        "fi": "Näytön UUID (numero)"
    },
    "playeriddesc": {
        "en": "Player UUID or empty (when using certificate)",
        "fi": "Näytön UUID tai tyhjä (jos käytetään sertifikaattia)"
    },
    "authuser": {
        "en": "Username:",
        "fi": "Käyttäjätunnus:"
    },
    "authuserexample": {
        "en": "user1",
        "fi": "käyttäjä1"
    },
    "authpass": {
        "en": "Password:",
        "fi": "Salasana:"
    },
    "auth": {
        "en": "Preset Authentication Settings",
        "fi": "Esiasetettu autentikaatio"
    },
    "authpassexample": {
        "en": "password",
        "fi": "salasana"
    },
    "authtarget": {
        "en": "Target site/page (URL):",
        "fi": "Kohdesivu tai -sivusto (URL):"
    },
    "authtargetexample": {
        "en": "http://example.com/",
        "fi": "http://sivusto.fi/"
    },
    "js": {
        "en": "Javascript Injection",
        "fi": "Javascript-liitokset:"
    },
    "jstarget": {
        "en": "Target site/page (URL):",
        "fi": "Kohdesivu tai -sivusto (URL):"
    },
    "jstargetexample": {
        "en": "http://example.com/page1.html",
        "fi": "http://sivusto.fi/sivu1.html"
    },
    "jscode": {
        "en": "Javascript:",
        "fi": "Javascript:"
    },
    "jscodeexample": {
        "en": "alert('hello world')",
        "fi": "alert('hello world')"
    },
    "colormap": {
        "en": "Default colormap",
        "fi": "Käyttöliittymän vakiovärit"
    },
    "blackonwhite": {
        "en": "Black on white",
        "fi": "Musta teksti, valkoinen tausta"
    },
    "whiteonblack": {
        "en": "White on black",
        "fi": "Valkoinen teksti, musta tausta"
    },
    "examplepresentationurl": {
        "en": "http://server/group/presentation.list, http://server/, https://server/",
        "fi": "http://palvelin/ryhma/esitys.list, http://server/, https://server/"
    },
    "x-characters-pw": {
        "en": "10 - 18 characters",
        "de": "10 - 18 Zeichen"
    },
    "description": {
        "en": "This is a description. You can write any text inside of this.<br>\
        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut\
        labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores\
        et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem\
        ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et\
        dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.\
        Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
        
        "de": "Melden Sie sich mit Ihrem Google-Konto in Google Chrome an, um Ihre personalisierten<br>\
        Browserfunktionen online zu speichern und über Google Chrome auf jedem Computer darauf<br>\
        zuzugreifen. Sie werden dann auch automatisch in Ihren Lieblingsdiensten von Google angemeldet.<br><br>"
    },
    "logout": {
        "en": "Logout",
        "de": "Abmeldung"
    },
    "enable": {
        "en": "Enable",
        "de": "Aktivieren"
    },
    "disconnect": {
        "en": "Disconnect:",
        "de": "Trennen:"
    }
};
