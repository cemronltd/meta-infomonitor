
function ServerPlayer() {
    var errorChecker = [];
    var erroredTab = null;
    var zoneListPath = "/docroot/infomonitor/jsp/zonelist_json.jsp";
    var url = null;
    var zoneconfig = null;
    var nextTab = null;
    var earlierTabs=[];
    var preloader = true;
    var currentTab = null;
    var missingPreloaderTab = false;
    var preloaderRetries=0;
    var zoneTab=null;
    var zoneUpdateListener=null;
    var preloaderTabWatcher=null;
    var restartInProgress=false;


    attachServerRotatorListener();

    this.getStartURL = function () {
        return url;
    };
    this.disablePreloader = function () {
        preloader = false;

    };

    this.getCurrentTab = function () {
        return currentTab;

    };
    this.getCurrentZoneTabId = function () {
        return zoneTab;
    };
    this.setCurrentZoneTabId = function (tabid) {
        zoneTab = tabid;
    };
    this.getZoneUpdateListener = function () {
        return zoneUpdateListener;
    };
    this.setZoneUpdateListener = function (listener) {
        zoneUpdateListener = listener;
    };

    this.preloaderEnabled = function () {
        return preloader;
    };
    this.startPreloaderWatcher = function () {
        console.log("Starting preloader tab watcher!");
        if (preloaderTabWatcher)clearInterval(preloaderTabWatcher);
        preloaderTabWatcher = setInterval(this.checkPreloader, 3400);

    };

    this.checkPreloader = function () {
        var m = (infomonitorTabs.length < 2);
        if (missingPreloaderTab && m && preloaderRetries == 20) {
            onErrorOccured({restartInfomonitor: true, description: "preloader tab missing"});
        } else {
            if (missingPreloaderTab && m) {
                console.log("Preloader tab might be missing, waiting...", preloaderRetries);
                preloaderRetries++;
            }
            else if (!missingPreloaderTab || !m) preloaderRetries = 0;
            missingPreloaderTab = m;
        }

    };
    this.checkPage = function (tabId, pageurl) {
        erroredTab = tabId;
        $.ajax({
            url: pageurl,
            success: function () {
                if (erroredTab) {
                    for (var i = 0; i < errorChecker.length; i++) {
                        var obj = errorChecker[i];
                        if (obj.tabId == erroredTab) {
                            clearInterval(obj.interval);
                            errorChecker.splice(i, 1);
                        }
                    }
                    reloadTab(erroredTab);
                }
            },
            error: function (err) {
            }
        });

    };

    this.clearTabErrors = function (tabid) {
        for (var i = 0; i < errorChecker.length; i++) {
            if (errorChecker[i].tabId == tabid) {
                clearInterval(errorChecker[i].interval);
                console.log("clearing interval:", errorChecker[i].interval);
                errorChecker.splice(i, 1);
                return;
            }
        }
    };
    this.clearAllErrors = function () {
        console.log("clearing all error checkers");
        for (var i = 0; i < errorChecker.length; i++) {
            clearInterval(errorChecker[i].interval);
            console.log("clearing interval:", errorChecker[i].interval);
        }
        errorChecker = [];
    };
    this.onErrorOccurred = function (errorDetails) {
        console.log("Error occured", errorDetails);
        if (errorDetails.restartInfomonitor) {
            if (restartInProgress)return;
            restartInProgress = true;
            //lets make sure that the process restarts
            console.log("onErrorOccured, forcibly restarting Infomonitor, error:", errorDetails);
            //shutdownPlayer();
            setTimeout(function () {
                restartPlayer();
            }, 1000);
            return;
        }
        if (errorDetails.error && errorDetails.error == 'net::ERR_ABORTED') {
            //console.log("Got error net::ERR_ABORTED, this error is skipped for now( due to Chrome misfires of this error)!", errorDetails);
            return;
        }
        /*if(settings.get("offlinemode")) {
         console.log("replacing with offline version", errorDetails.tabId, errorDetails.frameId);
         chrome.tabs.insertCSS(errorDetails.tabId, {
         file: "css/chrome_errors.css",
         allFrames:true
         }, function() {
         });
         if(errorDetails.frameId==0)
         replaceWithOfflineVersion(errorDetails.url, errorDetails.tabId);
         } else {
         console.log("Offline mode not enabled, let's show an error page");
         */
        chrome.tabs.get(errorDetails.tabId, function (errorTab) {
            if (errorTab.url.indexOf(settings.get("presentationurl")) == -1 && isInfomonitorTab(errorTab.id) && errorDetails.frameId == 0)
                chrome.cookies.get({url: settings.get("presentationurl"), name: "crn_im_nextpage"}, function (c) {
                    player.clearTabErrors(c.id);
                    var u = settings.get("presentationurl");
                    if (endsWith(u, "/")) u = u.substring(0, u.length - 1);
                    u += decodeURIComponent(c.value);
                    if (u.indexOf('autostart') == -1 && player.preloaderEnabled()) u += "&autostart=event";
                    chrome.tabs.update(errorTab.id, {url: u}, function (tab) {
                        if (tab.active)
                            setTimeout(function () {
                                chrome.tabs.executeScript(nextTab.id, {code: pageActivatedEvent, allFrames: true});
                            }, 15000);
                    });
                    chrome.cookies.remove({url: c.url, name: c.name});
                });
        });
        var existingCheck = false;
        for (var i = 0; i < errorChecker.length; i++) {
            if (errorChecker[i].tabId == errorDetails.tabId && errorChecker[i].url == errorDetails.url) existingCheck = true;
        }
        if (!existingCheck && (!errorDetails.error || errorDetails.error != 'net::ERR_ABORTED')) {
            console.log("Error occurred, running checkPage", errorDetails);
            var intvl = setInterval(function () {
                player.checkPage(errorDetails.tabId, errorDetails.url)
            }, 5000);
            errorChecker.push({interval: intvl, tabId: errorDetails.tabId, url: errorDetails.url});
        }
        //}
    };
    this.openViewWithZones = function (config) {
        if (!config.zones || config.zones.length == 0) {
            console.log("no zones defined, continuing without zones!");
            player.openViewWithoutZones();
            return;
        }
        player.zoneconfig = config;
        createTab({url: settings.get("presentationurl") + "docroot/infomonitor/html/player-zoneview.html", active: true}, function (tab) {
            player.setCurrentZoneTabId(tab.id);
            player.setCurrentTab(tab);
        });
    };
    this.setNextTab = function (tab) {
        if (nextTab) earlierTabs.push(nextTab);
        nextTab = tab;
    };
    this.preloadSlide = function (url, tab) {
        if (this.preloaderEnabled() && url && url.length > 0) {
            var srv = '';
            if (url.indexOf('http') != 0) {
                srv = settings.get("presentationurl");
                srv = srv.substring(0, srv.length - 1);
            }
            var u = srv + url + "&autostart=event";
//            if (!nextTab) {
            missingPreloaderTab = false;
            console.log("preloader tab create");
            createTab({url: u, active: false}, function (tab) {
                player.setNextTab(tab);
            });

        }
    };
    this.removeOlderTabs = function () {
        earlierTabs.forEach(function (t, index, array) {
            if (t && t.id != currentTab.id && (!nextTab || t.id != nextTab.id)) {
                removeTab(t.id);
                delete(earlierTabs[index]);
            }
        });

    };
    this.setCurrentTab = function (tab) {
        currentTab = tab;
    };
    this.rotateToNextTab = function () {
        if (this.preloaderEnabled()) {
            chrome.tabs.executeScript(nextTab.id, {code: beforeSlideActivation, allFrames: true});
            earlierTabs.push(currentTab);
            setTimeout(function () {
                chrome.tabs.update(nextTab.id, {active: true}, function (tab) {
                    player.setCurrentTab(tab);
                    setTimeout(function () {
                        player.removeOlderTabs();
                    }, 4000);
                    chrome.tabs.executeScript(nextTab.id, {code: slideActivation, allFrames: true});
                });
            }, 500);
        }
    };
    this.getZoneConfig = function () {
        return this.zoneconfig;
    };
    this.openViewWithoutZones = function () {
        console.log("openViewWithoutZones tab create");
        createTab({url: player.getStartURL(), active: true}, function (tab) {
            player.setCurrentTab(tab);
        });
    };

    this.shutdown = function () {
        this.clearAllErrors();
        clearStaleTabs();
        erroredTab = null;
        url = null;
        zoneconfig = null;
        nextTab = null;
        earlierTabs = [];
        currentTab = null;
        missingPreloaderTab = false;
        preloaderRetries = 0;
        zoneTab = null;
        checkRunning = false;
        zoneUpdateListener = null;
        if (preloaderTabWatcher)clearInterval(preloaderTabWatcher);

    };

    this.init = function() {
        //player = this;


        if (settings.get("IMRotator") == 'IMRotatorOneTab' || settings.get("IMRotator") == 'IMRotator') {
            console.log("Infomonitor: Starting in single tab mode!");
            player.disablePreloader();
        } else {
            console.log("Infomonitor: Starting in 2-tab (preloader) mode!");
            player.startPreloaderWatcher();
        }

        var setURL = settings.get("presentationurl");
        if (settings.get("PlayerMode") == 'PlayerModeScreen' && typeof(settings.get("PlayerUUID")) != 'undefined' && settings.get("PlayerUUID").length>0) {
            setURL += "start?scid=" + settings.get("PlayerUUID");
            url = setURL;
            if (endsWith(settings.get("presentationurl"), "/") && zoneListPath.charAt(0)=='/') zoneListPath = zoneListPath.substring(1);
            var zoneReqURL = settings.get("presentationurl") + zoneListPath + "?scid=" + settings.get("PlayerUUID") + "&_nocache=" + new Date().getTime();
            console.log("init launching openViewWith/outZones");
            $.ajax({
                url: zoneReqURL,
                success: player.openViewWithZones,
                error: player.openViewWithoutZones
            });

        } else {
            url = setURL;
            player.openViewWithoutZones();
        }
        restartInProgress=false;

    }
}

var imTabs;
var imSlides;

function ControlRoomPlayer() {

    var currentIMConfig;
    var currentTab;
    var slideChangeTimeout;
    var rotationReady;
    var fixedChangePoller;
    var retries = 0;
    var tabRefreshInterval = -1;
    var configRefreshInterval = -1;
    var tabRefreshJobs = [];
    var rotationPaused = false;
    var pauseNotification = null;
    var restartInProgress = false;
    var currentTab;

    attachServerRotatorListener();

    var loadConfig = function() {
        $.ajax({
            url:settings.get("presentationurl"),
            success: readConfig,
            error: configError
        });

    };
    var configError = function(xhr, status, error) {
        console.log("config error:", status, error);
        alert("Infomonitor configuration error. Message: '" + status + "'");
    };

    var isRotatedAlready = function(lastrun, currentTime) {
        if (!lastrun) return false;
        return ( lastrun.getSeconds() == currentTime.getSeconds() &&
            lastrun.getMinutes() == currentTime.getMinutes() &&
            lastrun.getHours() == currentTime.getHours()
            );
    };

    var checkTimeAndRotate = function() {
        if (rotationPaused) return;
        var config;
        var show;
        var c = new Date();
        for (var t in imTabs) {
            config = imTabs[t].config;
            show = config.showon;
            var lastrun = config.lastrun;
            if (show && !isRotatedAlready(lastrun, c)) {
                var tsMatch = false;
                if (show.seconds && show.seconds.length > 0) {
                    tsMatch = ($.inArray(c.getSeconds(), show.seconds) > -1);
                    //                if(tsMatch)console.log("seconds match",t,c.getSeconds());
                }
                if (show.minutes && show.minutes.length > 0) {
                    tsMatch = ($.inArray(c.getMinutes(), show.minutes) > -1);
                    //               if(tsMatch)console.log("minutes match",t,c.getMinutes());
                }

                if (show.hours && show.hours.length > 0) {
                    tsMatch = ($.inArray(c.getHours(), show.hours) > -1);
                    //             if(tsMatch)console.log("hours match",t,c.getHours());
                }
                if (tsMatch) {
                    config.lastrun = c;
                    rotate(parseInt(t));
                    break;
                }
            }
        }
    };

    var rotate = function(tabId, force) {
        if (rotationPaused && !force) return;
        if (!imTabs) {
            if (retries == 2) {
                console.log("tabs are not found, extension failed");
                return;
            }
            retries++;
            setTimeout(function() {rotate(tabId)}, 1000);
        }
        chrome.tabs.update(tabId, {active:true}, function(tab) {
            if (tab.url == 'about:crash' || tab.url == 'chrome://crash/') {
                console.log("tab crashed!" + tab.url);
                reloadTab(tabId);
            }
            player.setCurrentTab(tab);
        });
    };

    var insertTab = function (tab) {
        imTabs[tab.id] = {config: imSlides[tab.url], id: tab.id, index: tab.index};
        var r = tabRefreshInterval;
        var t = imSlides[tab.url].viewtime;
        if (t && t > 0) {
            r = t;
        }
        if (r > 0) {
            console.log("Initing periodic refresh for tab", tab.id, ", millis:", r);
            var intv = setInterval(function () {
                reloadTabIfInBackground(tab.id, {bypassCache: true});
            }, r);
            tabRefreshJobs.push(intv);
        }
    };

    var addDisplaySize = function (url) {
        return addDisplayDimensions(url, windowWidth, windowHeight);
    };

    var readConfig = function (configJSON) {
        if (fixedChangePoller) clearInterval(fixedChangePoller);
        fixedChangePoller = setInterval(checkTimeAndRotate, 100);
        imTabs = {};
        imSlides = {};
        /*currentIMConfig=JSON.parse(configJSON); */
        currentIMConfig = configJSON;
        if (currentIMConfig.viewtime && currentIMConfig.viewtime > 0) {
            tabRefreshInterval = currentIMConfig.viewtime;
        }
        var slide;
        var previousTabId;
        for (slide in currentIMConfig.slides) {
            var conf = currentIMConfig.slides[slide];
            conf.url = addDisplaySize(conf.url);
            imSlides[conf.url] = conf;

        }
        var i = 0;
        for (slide in imSlides) {

            createTab({url: imSlides[slide].url, active: (i == 0)}, function (tab) {
                insertTab(tab);
                if (tab.active) player.setCurrentTab(tab);
            });
            i++;
        }
    };

    var nextTab = function (currentTabObj, returnPrevious) {
        var tabid = currentTabObj.id ? currentTabObj.id : currentTabObj;

        if (!imTabs || imTabs[tabid]) {

            var slideConfig = imTabs[tabid].config;
            var tout = slideConfig.viewtime;
            var nextTab = 0;
            currentTab = tabid;
            var next = false;
            var i = 0;
            var t;
            var firstTab = 0;
            var previousTab = 0;

            for (t in imTabs) {
                if (i == 0) firstTab = t;
                if (next) {
                    returnPrevious ? nextTab = previousTab : nextTab = t;
                    break;
                }
                if (t == currentTab) {
                    next = true;
                } else {
                    previousTab = t;
                }
                i++;
            }
            if (next && !nextTab) nextTab = firstTab;
            return parseInt(nextTab);
        } else {
            console.log("Tab not part of rotation, configuration not found", currentTabObj, imTabs, imTabs[tabid]);
        }
        return null;
    };
    this.onTabUpdate = function (tabId, info, tab) {

        if (info.status == "complete") {
            if (tab.active && !rotationReady) {
                rotationReady = true;
                rotate(tabId);
            }

        }
    };

    this.preloaderEnabled = function () {
        return true;
    };
    this.setCurrentTab = function (tab) {
        currentTab = tab;
    };
    this.getCurrentTab = function () {
        if (!currentTab || !isInfomonitorTab(currentTab.id))  return null;
        return currentTab;

    };
    this.onErrorOccurred = function (errorDetails) {
        if (errorDetails.restartInfomonitor) {
            if (errorDetails.tabId) {
                reloadTab(errorDetails.tabId, null);
                return;
            } else {
                if (restartInProgress)return;
                restartInProgress = true;
                //lets make sure that the process restarts
                console.log("onErrorOccured, forcibly restarting Infomonitor");
                shutdownPlayer();
                setTimeout(function () {
                    start();
                }, 1000);
                return;
            }
        }
        if (errorDetails.error && errorDetails.error == 'net::ERR_ABORTED') {
            console.log("Got error net::ERR_ABORTED, this error is skipped for now( due to Chrome misfires of this error)!", errorDetails);
            return;
        }

        if (!errorDetails.frameId || errorDetails.frameId == 0) {
            chrome.tabs.insertCSS(errorDetails.tabId, {
                file: "css/chrome_errors.css"
            }, function () {
            });

            setTimeout(function () {
                reloadTab(errorDetails.tabId)
            }, 5000);
        }
    };

    this.init = function () {
        retries = 0;
        chrome.tabs.onUpdated.addListener(this.onTabUpdate);
        loadConfig();
        restartInProgress = false;
    };
    this.showNextTab = function () {
        this.pauseRotation();
        rotate(nextTab(currentTab), true);
    };
    this.showPreviousTab = function () {
        rotate(nextTab(currentTab, true), true);
    };
    this.continueRotation = function () {
        if (pauseNotification) {
            console.log(pauseNotification);
            pauseNotification.cancel();
            pauseNotification = null;
        }
        rotationPaused = false;
    };
    this.clearTabErrors = function () {
        // stub
    };
    this.pauseRotation = function () {
        if (pauseNotification) {
            console.log(pauseNotification);
            pauseNotification.cancel();
            pauseNotification = null;
        }


        rotationPaused = true;
        setTimeout(player.continueRotation, 5 * 60 * 1000);

        pauseNotification = window.webkitNotifications.createNotification(
            'img/Infomonitor-OSX-128.png',
            'Presentation paused!', // notification title
            'Press space to continue... '  // notification body text
        );
        pauseNotification.show();


    };
    this.toggleRotation = function() {


        if (rotationPaused) {
            this.continueRotation();
        }
        else {
            this.pauseRotation();

        }

    }

}
function serverRotatorMessageHandler(data, from, responseCallback) {
            switch (data.name) {
                case 'PlayerRequestACK':
                    chrome.tabs.executeScript(from.tab.id, {code:playerRequestAck,allFrames:true});
                    break;
                case 'PreloaderEnabled':
                    responseCallback(player.preloaderEnabled());
                    break;
                case 'GetOriginalWindowInnerHeight':
                    if(originalWindowInnerHeight==0) originalWindowInnerHeight=data.height;
                    responseCallback({height:originalWindowInnerHeight});
                    break;
                case 'LastPageReached':
                    CheckTabTTL();
                    break;
                case 'UpdateWindowInnerHeight':
                    originalWindowInnerHeight=data.height;
                    console.log("Original window inner height changed to:",data.height);
                    responseCallback({height:originalWindowInnerHeight});
                    break;
                case 'TabErrorNotPossible':
                    clearActiveTabError(from.tab);
                    break;
                case 'JS':
                    responseCallback(getJS(data.url));
                    break;
                case 'Settings':
                    responseCallback(settings.toObject());
                    break;
                case 'CustomCSS':
                    responseCallback(getCustomCSS(data.url, from.tab));
                    break;
                case 'ZoneConfig':
                    responseCallback(player.getZoneConfig());
                    break;
                case 'PluginCrashed':
                    responseCallback(pluginCrashHandler());
                    break;
                case 'ZoneEventsInit':
                    responseCallback(enableZoneEvents(data,from));
                    break;
                case 'StartURL':
                    responseCallback(player.getStartURL());
                    break;
                case 'ShowNextTab':
                    responseCallback(player.showNextTab());
                    break;
                case 'ShowPreviousTab':
                    responseCallback(player.showPreviousTab());
                    break;
                case 'ToggleRotation':
                    player.toggleRotation ? responseCallback(player.toggleRotation()) : responseCallback(notImplementedResponse);
                    break;
                case 'PreloadSlide':
                    player.preloadSlide ? responseCallback(player.preloadSlide(data.url, from.tab)) : responseCallback(notImplementedResponse);
                    break;
                case 'ChangeSlide':
                    player.rotateToNextTab ? responseCallback(player.rotateToNextTab(from.tab)) : responseCallback(notImplementedResponse);
                    break;
                case 'ChangePlaybackDuration':
                case 'PlaybackDurationChange':
                    console.log("######### ChangePlaybackDuration msg handling");
                    resetWatchdog(data.duration);
                    player.postMessage({event:"ChangePlaybackDuration", duration:data.duration});
                    break;
                default:
                   console.log("Background event handler not implemented, name:",data.name,",from:",from);



            }
}
function attachServerRotatorListener() {
    try {
        chrome.extension.onMessage.removeListener(serverRotatorMessageHandler);
    } catch (e) {
    }
    chrome.extension.onMessage.addListener(serverRotatorMessageHandler);
}
