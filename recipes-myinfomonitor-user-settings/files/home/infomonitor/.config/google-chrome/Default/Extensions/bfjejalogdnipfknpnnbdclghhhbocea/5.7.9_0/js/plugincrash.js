var pluginMaxErrorCount=5;
var pluginErrorCount=0;
var pluginChecker;

function checkFlashEmbeds(objs) {
    objs.filter(function(index) {
        console.log("checking flashes",$(this).attr("type"));
        if($(this).attr("type") && $(this).attr("type").indexOf("flash")>-1) return true;
    }).each(function(index){
        if(typeof this.SetVariable == 'function') {
            pluginErrorCount=0;
        } else {
            pluginErrorCount++;
        }
    });
}
function checkWMPEmbeds(objs) {
    objs.filter(function(index) {
        var t=$(this).attr("type");
        if(t && (t.indexOf("x-oleobject")>-1 || t.indexOf("application/x-ms-wm")>-1 || t.indexOf("application/x-ms-as")>-1)) return true;
    }).each(function(index){
        if(this.playState != 0) {
            pluginErrorCount=0;
            return false;
        } else {
            console.log("WMP plugin might be dead,", this, this.playState);
            pluginErrorCount++;
        }
    });
}
function checkQTEmbeds(objs) {
    objs.filter(function(index) {
        if($(this).attr("type") && $(this).attr("type").indexOf("quicktime")>-1) return true;
    }).each(function(index){
        if(typeof this.getDuration == 'function') {
            pluginErrorCount=0;
            return false;
        } else {
            console.log("QT plugin might be dead,", this, this.getDuration);
            pluginErrorCount++;
        }
    });
}
function checkMPlayerEmbeds(objs) {
    objs.filter(function(index) {
        if($(this).attr("type") && $(this).attr("type").indexOf("mplayer2")>-1) return true;
    }).each(function(index){
        if(typeof this.Play == 'function') {
            pluginErrorCount=0;
            return false;
        } else {
            console.log("MPlayer plugin might be dead, rechecking!");
            pluginErrorCount++;
        }
    });
}

function emitPluginCrashed() {

    chrome.extension.sendMessage({name: "PluginCrashed"}, function(response) {


    });
}
function checkEmbeds() {
    if(pluginErrorCount==pluginMaxErrorCount)  {
        console.log("Some plugin is dead, next page!");
        emitPluginCrashed();
        pluginErrorCount=0;
    } else {
        console.log("checking plugin crashes");
        var e=$("embed,object");
        checkFlashEmbeds(e);
        checkQTEmbeds(e);
        checkWMPEmbeds(e);
        checkMPlayerEmbeds(e);
        pluginChecker=setTimeout(checkEmbeds,3000);
    }
}
pluginChecker=setTimeout(checkEmbeds,3000);
