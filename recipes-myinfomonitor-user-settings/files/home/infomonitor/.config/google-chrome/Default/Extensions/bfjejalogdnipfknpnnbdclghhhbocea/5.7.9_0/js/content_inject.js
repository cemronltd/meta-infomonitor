/**
 *
 * Copyright (c) 2012 Cemron Ltd
 *
 **/

function injectCSS(cssuri, id) {
    if(!cssuri) return;
    var h=$(document).find("head")[0];
    var style = document.createElement('link');
    if (typeof(id)!='undefined') {
        var findEarlierById=$(document).find("#"+id);
        if(findEarlierById.length>0) {
            console.log("CSS injection with id ",id," already exists, aborting...");
            return;
        }
        style.setAttribute('id', id);

    }
    if (cssuri.indexOf('builtin:') == 0) {

        cssuri = chrome.extension.getURL("/css/webapps/" + cssuri.replace("builtin:", ""));
    }
    style.setAttribute('href', cssuri);
    style.setAttribute('rel', "stylesheet");
    style.type = 'text/css';
    h.appendChild(style);

}

function injectJS(code, id) {
    var jst = document.createElement('script');
    var src = false;
    if (typeof(id)!='undefined') {
        var findEarlierById=$(document).find("#"+id);
        if(findEarlierById.length>0) {
            console.log("JavaScript injection with id ",id," already exists, aborting...");
            return;
        }
        jst.setAttribute('id', id);
    }
    if (code.indexOf('builtin:') == 0) {
        src = true;
        code = chrome.extension.getURL("/js/webapps/" + code.replace("builtin:", ""));
    } else if (code.indexOf('http:') == 0) {
        src = true;
    }
    jst.setAttribute('type', "text/javascript");
    if (src) {
        jst.setAttribute('src', code);
    } else {
        $(jst).append(code);
    }
    $(document).find("head")[0].appendChild(jst);
}

chrome.extension.sendMessage({name: "JS", url: window.location.href}, function(response) {
        if (response && response != '')
            injectJS(response, 'infomonitor-js');
    }
);


chrome.extension.sendMessage({name: "CustomCSS", url: window.location.href}, function(response) {

            injectCSS(response, 'infomonitor-css');
    }
);








