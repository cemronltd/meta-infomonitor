window.addEvent("domready", function () {
    // Option 1: Use the manifest:
   /* new FancySettings.initWithManifest(function (settings) {
        settings.manifest.myButton.addEvent("action", function () {
            alert("You clicked me!");
        });
    });*/
    
    // Option 2: Do everything manually:

    var settings = new FancySettings(i18n.get("productsettings"));

    var optArray=[];


    var presentationurl_p = settings.create({
        "tab": i18n.get("playermodebasic"),
        "group": null,
        "name": "presentationurl",
        "type": "text",
        "label": i18n.get("presentationurl"),
        "text": i18n.get("examplepresentationurl")
    });
    optArray.push(presentationurl_p);

    var screenid_p = settings.create({
        "tab": i18n.get("playermodebasic"),
        "group": null,
        "name": "PlayerUUID",
        "type": "text",
        "label": i18n.get("playerid"),
        "text": i18n.get("playeriddesc")
    });
    optArray.push(screenid_p);



    var offline = settings.create({
        "tab": i18n.get("playermodebasic"),
        "group": null,
        "name": "offlinemode",
        "type": "checkbox",
        "label": i18n.get("offlinemode"),
        "value": true
    });

    var ttl = settings.create({
        "tab": i18n.get("advanced"),
        "group": null,
        "name": "ttl",
        "type": "text",
        "label": i18n.get("ttl"),
        "text": "1000"
    });
    optArray.push(ttl);

    
    myButton.addEvent("action", function () {
        alert("You clicked me!");
    });
    
    settings.align(optArray);
});
