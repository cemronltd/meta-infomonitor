var zonesInited = false;
var zoneInitCount = 0;

function createZone(idx, c) {
    var f = document.createElement('iframe');
    f.setAttribute('id', "im-ch-main-" + idx);
    f.setAttribute('class','im-zone');
    f.setAttribute('src', c["url"]);
    f.setAttribute('frameborder', '0');
    f.setAttribute('scrolling', 'no');
    f.setAttribute('marginwidth', '0');
    f.setAttribute('marginheight', '0');
    var fj = $(f);
    fj.css("position", "absolute");
    fj.css("z-index","2001");
    //console.log("createZone",c);
    if (c.styles) {
        $.each(c.styles, function(k, v) {
            if (v && v != "") {
                fj.css(k, v);
            }
        });
        $(document).find("body")[0].appendChild(f);
    }  else {
        console.log("no styles defined for iframe"+config[url]);
    }
}

function loadZones() {
    if (!zonesInited) {
        zonesInited = true;
        chrome.extension.sendMessage({name: "ZoneConfig", url: window.location.href}, function(response) {
            if (response && response != '') {

                zonesInited = true;
               // console.log("Zones",response);
                $.each(response, createZone);

            } else {
                if (zoneInitCount < 10) {
                    setTimeout(loadZones, 100);
                    zoneInitCount++;
                }
            }
        });
    }
}
function reloadZones() {
    console.log("Reloading zones");
    $(".im-zone").remove();
    zonesInited=false;
    zoneInitCount=0;
    setTimeout(function(){
        loadZones();
    },5);

}
chrome.extension.sendMessage({name: "ZoneEventsInit", url: window.location.href}, function(response) {
});

