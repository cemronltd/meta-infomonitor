
function URLConnectionHealthChecker() {

    var self=this;
    var checkerInterval;
    var offlineURLs={};
    var IMServerOnline=true;

    this.setIMServerOffline = function () {
        IMServerOnline = false;
    };
    this.setIMServerOnline = function () {
        IMServerOnline = true;
    };
    this.offlineURLs = function () {
        return offlineURLs;
    };
    this.isOffline = function (url) {
        if (!navigator.onLine || !IMServerOnline) return true;
        console.log("urlconnection_health_checker.js: navigator and server marked online, url offline:", offlineURLs.hasOwnProperty(url));
        return offlineURLs.hasOwnProperty(url);
    };
    this.isOnline = function (url) {
        return (!this.isOffline(url)) && IMServerOnline;
    };

    this.setOnline = function (url) {
        if (!offlineURLs.hasOwnProperty(url)) return;
        console.log(url, "back online!");
        var obj = offlineURLs[url];
        if (obj && obj["checker"]) clearInterval(obj["checker"]);
        //if(player && player.reloadFrame) player.reloadFrame(url);
        delete offlineURLs[url];
    };

    this.setOffline = function (url, tabId) {
        if (offlineURLs.hasOwnProperty(url)) return;
        var obj = {tabId: tabId};
        checkTask(self, url);
        var c=setInterval(function () {
            checkTask(self, url);
        }, 30000);
        obj["checker"] = c;
        offlineURLs[url] = obj;
        //if(player && player.reloadFrame) player.reloadFrame(url, true);
    };


    var checkTask=function(from,url) {
       console.log("checking if url still offline...",url);
        var jqxhr = $.ajax({
            type: 'GET',
            url: url,
            timeout: 5000
            } )
            .done(function() { from.setOnline(url); });

       if(self.offlineURLs().hasOwnProperty(url)) {
           var up=self.offlineURLs()[url];
        try {
            if(up.hasOwnProperty("xhr")) up.xhr.abort();
        } catch(e) {
            console.log(e);
        }
           up["xhr"]=jqxhr;
       }
    }


}


