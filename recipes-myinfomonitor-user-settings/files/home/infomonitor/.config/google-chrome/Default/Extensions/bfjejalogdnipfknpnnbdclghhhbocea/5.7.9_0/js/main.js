var mainContentLoading = "var evt=document.createEvent('Events');evt.initEvent('cim-maincontentloading',true,true);window.dispatchEvent(evt);";
var mainContentComplete = "var evt=document.createEvent('Events');evt.initEvent('cim-maincontentcomplete',true,true);window.dispatchEvent(evt);";
var pluginCrashed = "var evt=document.createEvent('Events');evt.initEvent('cim-plugincrashed',true,true);window.dispatchEvent(evt);";
var playerRequestAck = "var evt=document.createEvent('Events');evt.initEvent('cim-playercontrolack',true,true);window.dispatchEvent(evt);";
var beforeSlideActivation = "var evt=document.createEvent('Events');evt.initEvent('cim-beforeslideactivation',true,true);window.dispatchEvent(evt);";
var slideActivation = "var evt=document.createEvent('Events');evt.initEvent('cim-slideactivation',true,true);window.dispatchEvent(evt);";

var inited = false;
var player;
var screenWidth = 1366;
var screenHeight = 768;
var zoneEventsInited = false;
var checkRunning = false;
var tabCheckRunning = false;
var tabHealthCheckerInterval;
var staleTabs={};
var tabHealthCheckerCheckInterval;
var tabTTL;
var settingsEditInProgress=false;
var listenersInited=false;
var originalWindowInnerHeight=0;
var extensionTab;
var playerStopped=false;
var defaultTTL=1000;
var oStorage;
var connectionHealthChecker;
var tabPreventer=null;

var notImplementedResponse = {success:false, notimplemented:true};
var defaultSettings = {
    'IMRotator': 'IMRotator',
    'PlayerMode': 'PlayerModeScreen',
    'Colormap': 'blackonwhite'
};
var settings = new Store("settings", defaultSettings);
var infomonitorTabs = [];

var blacklistedURLs=[];
blacklistedURLs.push("woopra.com");
blacklistedURLs.push("google-analytics.com");
blacklistedURLs.push("snoobi.com");


function cleanUpSlideURL(url) {
    if(typeof(url)==='undefined' || !url) return url;
    url=url.replace(/[?&]w=[0-9]*/,'');
    url=url.replace(/[?&]h=[0-9]*/,'');
    url=url.replace(/[?&]d=[0-9]*/,'');
    url=url.replace(/[?&]__im_offline_cache_id=[a-zA-Z0-9]*/,'');
    return url;

}
function adler32(a){
    for(var b=65521,c=1,d=0,e=0,f;f=a.charCodeAt(e++);d=(d+c)%b)c=(c+f)%b;return d<<16|c
}

function endsWith(s, pattern) {
    var d = s.length - pattern.length;
    return d >= 0 && s.lastIndexOf(pattern) === d;
}
function execFunc(functionName, context /*, args */) {
  var args = Array.prototype.slice.call(arguments).splice(2);
  var namespaces = functionName.split(".");
  var func = namespaces.pop();
  for(var i = 0; i < namespaces.length; i++) {
    context = context[namespaces[i]];
  }
  return context[func].apply(this, args);
}


function isBlacklistedURL(url) {
    for(var i=0;i<blacklistedURLs.length;i++) {
        if(url.indexOf(blacklistedURLs[i])>-1) {
            console.log("Blacklisted URL:",url);
            return true;
        }
    }
    return false;
}
function pluginCrashHandler() {
    console.log("Plugin crash dialog, restarting");
    if(player) player.onErrorOccurred({restartInfomonitor:true});
  /*  console.log("Emitting Plugin Crashed signal");
    var tabid=null;
    var c=serverPlayer.getCurrentTab();
    if(c) tabid=c.id;
    chrome.tabs.executeScript(tabid, {code:pluginCrashed,allFrames:true}); */
}
function settingsTabHandler(tabId) {
    if(playerStopped) return;
    chrome.tabs.get(tabId,function(tab) {
        //console.log("onActivated", tab.title, tab.url, tab.id);
        if(tab.title == 'Infomonitor Settings'
           || tab.url.indexOf("chrome://extensions-frame")>-1
           || tab.url.indexOf("chrome://chrome/extensions")>-1
           || tab.url.indexOf("chrome://chrome/settings")>-1
           || tab.url.indexOf("chrome://plugins")>-1
           || tab.url.indexOf("chrome://flags")>-1
           || tab.url.indexOf("chrome://net-internals")>-1
           || tab.url.indexOf("chrome://gpu")>-1)
        {
            console.log("Settings edited, stopping player");
            shutdownPlayer();
            settingsEditInProgress=true;
        }
    });

}

function authRequestHandler(details) {
    console.log("authRequestHandler called!");
    for (var i = 1; i <= 100; i++) {
        var at = settings.get("authtarget" + i);
        var au = settings.get("authuser_" + i);
        var ap = settings.get("authpass_" + i);
        if (!at || at == "") {
            break;
        }
        var att=at.replace('*','');
        if (details.url.indexOf(att)>-1)   {
                console.log("Found credentials for ",details.url);
                return {authCredentials:{username:au, password:ap}};
        }
    }
    console.log("Auth not found, cancelling request for url",details.url);
    return {cancel:true};
}

function clearStaleTabs() {
    if(tabHealthCheckerCheckInterval) clearInterval(tabHealthCheckerCheckInterval);
    staleTabs={};
}

function createTab(details, callback) {

    chrome.tabs.create(details, function(tab) {
        console.log("create tabs:",tab);
        infomonitorTabs.push(tab);
        console.log("createTab", infomonitorTabs);
        if(callback) callback(tab);
    });
}
function removeTab(tabId, callback) {
    chrome.tabs.remove(tabId, function(tab) {
        for (var i = 0; i < infomonitorTabs.length; i++) {
            var a = infomonitorTabs[i];
            if (a.id == tabId) {
                infomonitorTabs.splice(i, 1);
                console.log("removed tab",tabId,infomonitorTabs);
                break;
            }
        }
        if(callback) callback(tab);
    });
}
function shutdownPlayer() {
    if(player && !playerStopped) {
        if(player.onErrorOccured)chrome.webNavigation.onErrorOccurred.removeListener(player.onErrorOccurred);
        if(player.shutdown) player.shutdown();

        if(tabHealthCheckerCheckInterval) {
            console.log("clearing tab health checker internal interval");
            clearInterval(tabHealthCheckerCheckInterval);
        }
        if(tabHealthCheckerInterval) {
            console.log("clearing tab health checker interval");
            clearInterval(tabHealthCheckerInterval);
        }

    }
    RemoveAllIMTabs();
    playerStopped=true;
    inited=false;
}
function restartPlayer() {
    start();
}
function RemoveAllIMTabs(callback) {

    chrome.windows.getAll({populate:true}, function(arr) {
        $.each(arr, function(i,win) {
            if(win.tabs)
                $.each(win.tabs,function(j,tab){
                    if(isInfomonitorTab(tab.id)) {
                        if(player.clearTabErrors) player.clearTabErrors(tab.id);
                        console.log("closing:", tab.url);
                        removeTab(tab.id);
                    }
                });

        });
        if(typeof(callback)=='function') callback();

    });
    tabCheckRunning=false;

}
function removeAllOtherTabs(callback) {
    removeAllOtherTabsWithForce(false,callback);
}

function removeAllOtherTabsWithForce(force, callback) {
    chrome.windows.getAll({populate:true}, function(arr) {

           $.each(arr, function(i,win) {
               if(win.tabs && win.tabs.length>1)
                   $.each(win.tabs, function(j,tab){
                       if(!isInfomonitorTab(tab.id)  && (force || tab.url.indexOf("pages/start.html")==-1 && tab.url != 'chrome://newtab/' && tab.url != 'chrome://extensions/'  && tab.url != 'about:blank' && tab.url!='')) {
                           console.log("removing tab:",tab.url);
                           removeTab(tab.id);
                       }
                   });

           });
           if(typeof(callback)=='function') callback();
       });

}
function clearActiveTabError(tab) {
    if(typeof(tab.id)!=='undefined') staleTabs[tab.id]=null;
}
function tabHealthChecker() {
    if(tabCheckRunning || !player) return;
    tabCheckRunning=true;
    try {
    var c=player.getCurrentTab();
    if(c){
        chrome.tabs.get(c.id, function(cTab) {
            try {
                var u=cTab.url;
            } catch(e) {
                console.log("Current tab is missing! Restarting player, error:",e);
                player.onErrorOccurred({restartInfomonitor:true, description:"tab missing"});
                return;
            }
            if(cTab.url.indexOf("chrome") == -1 && cTab.url.indexOf("file") == -1){
                if(!staleTabs[cTab.id]) {
                    console.log("Checking tab",cTab.id);
                    staleTabs[cTab.id]={count:0};
                } else {
                    console.log("Tab not responding, declared as dead:",cTab.id, cTab.active, cTab.url);
                    player.onErrorOccurred({restartInfomonitor:true, tabId:cTab.id});
                    console.log("Error emitted, stopping tab health check!");
                    clearInterval(tabHealthCheckerCheckInterval);
                    clearActiveTabError(cTab);
                }
                chrome.tabs.executeScript(cTab.id, {file:"js/status/tab_health_checker.js"}, function() {
                });
            }
        });
    } else {
        console.log("tabHealthChecker: tab missing,",c);
        player.onErrorOccurred({restartInfomonitor:true, description:"tab missing"});
    }
    } catch(err) {
        console.log("Health checker error:",err);
        tabCheckRunning=false;

    }
    tabCheckRunning=false;
}
function isInfomonitorTab(tabid) {
    for (var i = 0; i < infomonitorTabs.length; i++) {
        var a = infomonitorTabs[i];
        if (a.id == tabid) {
            return true;
        }
    }
    console.log("tab",tabid,"is not an InfomonitorTab");
    return false;
}
function addDisplayDimensions(url, w, h) {
    if (url.indexOf('?') == -1)
        url += "?";
    else
        url += "&";
    url += "w=" + w;
    url += "&h=" + h;
    return url;

}


function reloadTabIfInBackground(tabId) {
    chrome.tabs.get(tabId, function(tab){
       if(!tab.active) {
           reloadTab(tab.id,null);
       }
    });

}
function reloadTab(tabId, f) {
    chrome.tabs.reload(tabId, {bypassCache:true}, function() {
        if(f) f();
    });
}
function enableZoneEvents(params,from) {
    if(zoneEventsInited) return;
    zoneEventsInited=true;
    console.log("enableZoneEvents:",params,from);
    chrome.webNavigation.onBeforeNavigate.addListener(function(details) {
        if(details.frameId < 3) {
             if(details.frameId < 3) chrome.tabs.executeScript(details.tabId, {code:mainContentLoading,allFrames:true});
         }
     });
    chrome.webNavigation.onCompleted.addListener(function(details) {
        // dirty way of filtering events only for main zone frame
        if(details.frameId < 3) {
             if(details.frameId < 3) chrome.tabs.executeScript(details.tabId, {code:mainContentComplete,allFrames:true});
         }
     });
}
function CheckTabTTL() {
    if(player.preloaderEnabled()) return;
    if(tabTTL>0) {
        console.log("Decreasing tab TTL to ",tabTTL-1);
        tabTTL--;
    } else {
        tabTTL=settings.get("tabttl")?settings.get("ttl"):defaultTTL;
        if(player) player.onErrorOccurred({restartInfomonitor:true});
    }

}
var wdEarlierTimestamp=-1;
var wdMinimumResetInterval=1000;
var wdEarlierTimeout=-1;

function resetWatchdog(timeout) {
    var timeNow=new Date().getDate();
    if(wdEarlierTimestamp!=-1 && wdEarlierTimeout!=-1 && timeout===wdEarlierTimeout && (timeNow-wdEarlierTimestamp)<wdMinimumResetInterval){
        console.log("FIXME: Skipping identical reset for watchdog:", timeout);
    }
    wdEarlierTimeout=timeout;
    wdEarlierTimestamp=timeNow;
    $.get('http://127.0.0.1:2310/wd/ping/chrome',{t:(timeout?timeout:61234)}, function(data) {
    });

}
function globalMessageHandler(data, from, responseCallback) {
        switch (data.name) {
            case 'ResetWatchdog':
                resetWatchdog(data.newTimeout);
                break;
        }
}

function attachListeners() {
    if(listenersInited) return;
    console.log("Attaching listeners");
    listenersInited=true;

    chrome.webRequest.onAuthRequired.addListener(player.authRequestHandler, {urls: ["<all_urls>"]}, ["blocking"]);


    chrome.tabs.onRemoved.addListener(function (id,info) {
        var infomonitorTabRemoved=false;
        for (i = 0; i < infomonitorTabs.length; i++) {
            var a = infomonitorTabs[i];
            if (a.id == id) {
                infomonitorTabs.splice(i, 1);
                infomonitorTabRemoved=true;
                break;
            }
        }
        if(!infomonitorTabRemoved && settingsEditInProgress) {
            settingsEditInProgress=false;
            start();
        }
    });
    //chrome.webRequest.onBeforeRequest.addListener(function(details){
    //    console.log("Cancelling",details);
    //    return {cancel: true};
    //},
    //{urls: ["http://*/*","https://*/*"]},
    //["blocking"]);
    chrome.extension.onMessage.addListener(globalMessageHandler);

// checker for runaway tabs (e.g. tabs which no longer point to original Infomonitor Server, breakout iframe code etc.)

    chrome.webRequest.onBeforeRequest.addListener(
        function(details) {
            var pu=settings.get("presentationurl");
            if(!pu || pu.length==0) return {cancel: false };
            pu=pu.substring(pu.indexOf("://"));
            var ou=details.url.substring(details.url.indexOf("://"));
            //console.log("onBeforeRequest - should we cancel?",details.tabId,details.frameId==0,details.url.indexOf("http")==0,ou.indexOf(pu) == -1);
            //console.log("onBeforeRequest - details:",details,ou,pu);
            if((details.tabId && !isInfomonitorTab(details.tabId) || settings.get("PlayerMode") != 'PlayerModeScreen'))  return { cancel:false };
            return {cancel: (details.tabId && details.frameId==0 && details.url.indexOf("http")==0 && ou.indexOf(pu) == -1)};
        },
        {urls: ["<all_urls>"], types:["main_frame"]},
        ["blocking"]);

    chrome.webRequest.onBeforeRequest.addListener(
        function(details){
            if(isBlacklistedURL(details.url)) return { cancel: true };
            if(details.url.indexOf('127.0.0.1:2310')==-1 &&
               settings.get("offlinemode") &&
               details.url.indexOf('__im_offline_cache_id')==-1 &&
               connectionHealthChecker.isOffline(details.url) &&
               details.url.indexOf('&__im_offline=false')==-1) {
                var url=details.url;
                console.log("onBeforeRequest", url);
                var r=oStorage.getURLSync(url);
                if(typeof(r)=='string') {
                    r=oStorage.getRedirectURL(url);
                    console.log("Redirecting to",r);
                    return {redirectUrl:r};
                } else {
                    console.log("Offline copy not found for:",details.url,r);
                }
            } else {
            }
        },
        {urls: ["http://*/*","https://*/*"], types: ["main_frame", "sub_frame", "stylesheet", "script", "image", "object", "xmlhttprequest", "other"]},
        ["blocking"]
    );
    var firstCompletedRequest=true;

    chrome.webRequest.onCompleted.addListener(
      function(details) {
        if(details.url.indexOf('127.0.0.1:2310')==-1 && settings.get("offlinemode") && details.tabId>-1 && details.url.indexOf('__im_offline_cache_id')==-1) {
            var responseURL=details.url;
            var responseHeaders=details.responseHeaders.slice(0);
            var fromCache=details.fromCache;
            oStorage.getStatus(responseURL, function(url,isExpired) {
                //console.log("onCompleted: url ",url," isExpired:",isExpired)
                if(isExpired) {
                    if(!fromCache && firstCompletedRequest) {

                       //oStorage.truncateStorage(function() {
                            oStorage.queueURLForFetching(url,responseHeaders);
                       //});
                    } else {
                        oStorage.queueURLForFetching(url,responseHeaders);
                    }
                    firstCompletedRequest=false;
                }  else {
                }
            });
        } else {
        }


      },
      {urls: ["http://*/*","https://*/*"]},
      ["responseHeaders"]);

}

chrome.tabs.onCreated.addListener(function(tab) {
    settingsTabHandler(tab.id);
});
chrome.tabs.onActivated.addListener(function(activeInfo) {
    settingsTabHandler(activeInfo.tabId);
});

function preventNewTabs() {
    if(tabPreventer) clearInterval(tabPreventer);
    tabPreventer=setInterval(function() {
        removeAllOtherTabsWithForce(true);
        },60000);
}
function start() {
    if (!inited) {
        inited=true;
        resetWatchdog(30111);
        var d=new Date().getTime();
        console.log("--- EXTENSION MODE:",settings.get("extensionmode"));
        connectionHealthChecker = new URLConnectionHealthChecker();
        oStorage=new OfflineStorage(function() {
            console.log("OfflineStorage inited, going for player init, elapsed:", new Date().getTime()-d+"ms");

            player = new ChromePlayer();
            RemoveAllIMTabs(removeAllOtherTabs(function(){
                attachListeners();
                player.start(settings);
            }));

            preventNewTabs();
            if(player) {
                chrome.webNavigation.onErrorOccurred.addListener(player.onErrorOccurred);
            }
        });

    }
}
function restart() {
    if(oStorage) delete oStorage;
    if(player) {
        if(player.stop) player.stop();
        delete player;
    }
    if(connectionHealthChecker) delete connectionHealthChecker;
}


window.addEventListener('load',function(evt) {
       screenWidth = this.screen.width;
       screenHeight = this.screen.height;
       setTimeout(start,1000);
});



window.addEventListener('error', function (evt) {
    console.log("###################### ERROR: Caught[via 'error' event]:  '" + evt.message + "' from " + evt.filename );
    console.log(evt);
    if(chrome.runtime.reload) {
        //chrome.runtime.reload();
    } else {
        //restart();
    }
    evt.preventDefault();
});





