var SlideLoader = function (config) {
    this.config = config;
    this.slideConfig = {};
    this.slides = [];
    this.fs = null;
    var self = this;
    var checkInterval;
    this.configChecksum = null;
    this.shouldReloadConfig = false;
    this.loadingConfig = false;

    this.startConfigChecker = function (from) {
        if (checkInterval) clearInterval(checkInterval);
        checkInterval = setInterval(function () {
            self.loadFrames(from);
        }, 5000);
    }


};

SlideLoader.prototype.loadFrames = function (obj, callback) {

    var self = this;

    var endsWith = function (str, suffix) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    };
    var getURL = function (presentationURL, UUID) {
        var u = presentationURL;
        if (!endsWith(presentationURL, '/')) u += "/";
        u += "player?scid=" + UUID + "&__im_offline=false";
        return u;
    };

    var readConfig = function (configJSON, status, xhr) {
        self.loadingConfig = false;
        if (self.fs == null) throw "fsMissing";
        var configJSONText = xhr.responseText;
        var newCS = adler32(configJSONText);
        if (settings.get("offlinemode")) connectionHealthChecker.setIMServerOnline();
        if (self.configChecksum && newCS === self.configChecksum && !self.shouldReloadConfig) {
            if (callback) callback({player: obj, loaded: true, updated: false});
            return;
        }

        self.fs.root.getFile("ChromePlayerConfig.json", {create: true, exclusive: false}, function (fileEntry) {
            fileEntry.file(function (file) {
                var reader = new FileReader();
                reader.onloadend = function (e) {
                    self.configChecksum = adler32(this.result);
                    console.log("Should we refresh config? checksums differ=", (newCS != self.configChecksum), ", shouldReloadConfig=", (self.shouldReloadConfig));
                    if (newCS != self.configChecksum || self.shouldReloadConfig) {
                        self.shouldReloadConfig = false;
                        fileEntry.remove(function () {
                            self.fs.root.getFile("ChromePlayerConfig.json", {create: true, exclusive: false}, function (fileEntry2) {
                                fileEntry2.createWriter(function (fw) {
                                    var blob = new Blob([configJSONText], {type: 'text/plain'});
                                    fw.write(blob);
                                    obj.onConfigChange();
                                });
                            });

                        });
                    }
                };
                reader.readAsText(file);
            });
        }, function (e) {
            console.log("File reading failed", e)
        });
        self.slideConfig = configJSON;
        console.log("config from server", self.slideConfig);
        if (callback) callback({player: obj, loaded: true, updated: true});

    };
    var configError = function (err) {
        self.loadingConfig = false;
        self.fs.root.getFile("ChromePlayerConfig.json", {create: true, exclusive: false}, function (fileEntry) {
            fileEntry.file(function (file) {
                var reader = new FileReader();
                reader.onloadend = function (e) {
                    console.log("Config load error, using cached version:", err);
                    if (settings.get("offlinemode")) {
                        console.log("Config load error, setting player to offline state");
                        connectionHealthChecker.setIMServerOffline();
                    }
                    self.slideConfig = JSON.parse("" + this.result);
                    self.shouldReloadConfig = true;
                    if (callback) callback({player: obj, loaded: true, updated: false});
                };
                reader.readAsText(file);
            });
        }, function (e) {
            console.log("File reading failed", e)
        });
    };
    var startConfigQuery = function () {
        if (self.loadingConfig) {
            return;
        }
        self.loadingConfig = true;
        setTimeout(function () {
            $.ajax({
                url: getURL(self.config.get("presentationurl"), self.config.get("PlayerUUID")),
                success: readConfig,
                error: configError
            });
        }, 1000);
    };
    if (!self.fs) {
        window.webkitStorageInfo.requestQuota(window.PERSISTENT, 10 * 10 * 1024, function (grantedBytes) {
            window.webkitRequestFileSystem(window.PERSISTENT, grantedBytes, function (fs) {
                self.fs = fs;
                startConfigQuery();
            });
        }, function (e) {
            console.log('Config storage init failed', e);
        });
    } else {
        startConfigQuery();
    }

};


function ChromePlayer() {

    var loader;
    var port_fe;
    var tabId;
    var currentSlideTimeout = null;
    var currentSlide = null;
    var initialConfig = true;
    var self = this;
    var zoneConfig = null;
    var controlMessageCheckerJob = null;
    this.playerControlMessageLog = {};
    var restartInProgress = false;


    attachChromePlayerListener();
    this.stop = function () {
        if (controlMessageCheckerJob) clearTimeout(controlMessageCheckerJob);
        if (currentSlideTimeout) clearTimeout(currentSlideTimeout);
        initialConfig = true;
        loader = null;
        restartInProgress = true;
    };

    this.getConfig = function () {
        return loader.slideConfig;
    };
    this.startPlayback = function () {
    };
    this.messageListener = function (msg) {
        //console.log("fe -> bg message received",msg);
        if (msg.rpc) {
            execFunc(msg.rpc.func, window, msg.rpc.args);
        }
    };
    this.portListener = function (port) {
        // console.log("port listener called",port);
        port_fe = port;
        console.assert(port.name == "im.ports.chromeplayer.frontend");
        port_fe.onMessage.addListener(player.messageListener);
    };
    this.setTab = function (_tabId) {
        tabId = _tabId;
    };
    this.postMessage = function (msg) {
        var self = this;
        if (typeof port_fe == 'undefined') {
            setTimeout(function () {
                self.postMessage(msg)
            }, 100);
            return;
        }
        port_fe.postMessage(msg);
    };
    this.execFrontEndFunction = function (func, args) {
        this.postMessage({rpc: {func: func, args: args}});
    };
    this.triggerFrontEndEvent = function (event, details) {
        var d = details;
        if (!d) details = {};
        player.postMessage({event: {name: event, details: details}});

    };
    this.initComms = function (callback) {
        chrome.extension.onConnect.addListener(this.portListener);
        setTimeout(function () {
            //chrome.extension.getURL("/pages/player.html")
            //settings.get("presentationurl")+"docroot/infomonitor/html/player.html"
            createTab({url: chrome.extension.getURL("/pages/player.html"), active: true}, function (tab) {
                player.setTab(tab.id);
                callback();
            });
        }, 2000);
    };
    this.sendConfig = function () {

        // make sure that our state is valid when config gets reloaded
        console.log("Config load request, clearing currentSlide and current slide timeout");
        if (currentSlideTimeout) clearTimeout(currentSlideTimeout);
        currentSlide = null;
        var conf = player.getConfig();
        console.log("Sending config to fe", conf);
        setTimeout(function () {
            player.execFrontEndFunction("player.loadConfig", conf);
        }, 1000);
    };
    this.updateNetworkStatus = function () {
        self.execFrontEndFunction("player.setNetworkStatus", connectionHealthChecker.isOnline());
    };
    this.start = function (settings) {

        loader = new SlideLoader(settings);
        loader.loadFrames(this, function (status) {
            initialConfig = false;
            if (status.loaded) {

                self.initComms(function () {
                    self.execFrontEndFunction("IM_ch_initPlayer");
                    self.updateNetworkStatus();
                    setTimeout(function () {
                        loader.startConfigChecker(self);
                    }, 20000);
                });
            } else {
                console.log("Server comms failed, error:", status.error);
                self.updateNetworkStatus();
            }

        });

    };
    this.getZoneConfig = function () {
        return zoneConfig;
    };
    this.restart = function () {
        console.log("restart called");

        self.sendConfig();
    };
    this.reloadFrame = function (frameURL, forceReload) {
        if (frameURL.url) frameURL = frameURL.url;
        console.log("player backend: reloading frame", frameURL, forceReload);
        var reload = true;
        if (!frameURL || frameURL.indexOf("http") == -1) {
            console.log("reloadFrame: frameURL empty", frameURL);
            return;
        }
        if (!forceReload && settings.get("offlinemode") && connectionHealthChecker.isOffline(frameURL)) {
            console.log("reloadFrame: Content offline, not reloading:", frameURL);
            return;
        }
        player.execFrontEndFunction("player.reloadFrame", {url: frameURL});
        //self.onPlayerControlMessage({name:"ReloadFrame", details:{URL:frameURL}});
    };

    this.restartCurrentSlideTimeout = function (data) {
        var duration = data;
        if (data.duration) duration = data.duration;
        resetWatchdog(duration);
        if (currentSlideTimeout) clearTimeout(currentSlideTimeout);
        currentSlideTimeout = setTimeout(function () {
            player.execFrontEndFunction("player.showNextSlide");
        }, duration);
    };
    this.setCurrentSlide = function (slide) {
        console.log("Setting current slide", slide);
        currentSlide = slide;
    };
    this.getCurrentSlide = function () {
        return currentSlide;
    };
    this.onConfigChange = function () {
        if (!initialConfig) {
            this.restart();
        }
        initialConfig = false;
    };
    this.isExternalEventSource = function (sourceUrl) {
        var source=cleanUpSlideURL(sourceUrl);
        console.log("testing if the event is from an external source", source, loader.slides);
        loader.slides.forEach(function(element,idx,arr) {
            var url = cleanUpSlideURL(element["url"]);
            if ( url === source ) {
                console.log("Internal event source", source, ", matching slide:", url);
                return false;
            }
        });
        console.log("External event source", sourceUrl);
        return true;
    };

    this.onPlaybackDurationChange = function (details) {

        if (cleanUpSlideURL(details.url.href) == cleanUpSlideURL(currentSlide) || this.isExternalEventSource(details.url.href)) {
            console.log("Received Duration Change for Active Slide:", currentSlide, details.duration);
            // only restart counter when event is for current slide
            resetWatchdog(details.duration);
            this.restartCurrentSlideTimeout(details.duration);
        }
        player.execFrontEndFunction("player.updateSlideDuration", {url: details.url.href, duration: details.duration});

    };
    this.sendMessage = function (data, callback) {
        try {
            console.log("Sending message:", data.name, data.details);
            chrome.extension.sendMessage({name: data.name, details: data.details}, function (response) {
                if (callback) callback(response);
            });
        } catch (e) {
            console.log("send failed");
            setTimeout(function () {
                player.sendMessage(data, callback);
            }, 50);
        }
    };

    var playerControlMessageChecker = function () {
        for (var msg in self.playerControlMessageLog) {
            var val = self.playerControlMessageLog[msg];
            var errCount = val.errCount;
            if (!errCount) self.playerControlMessageLog[msg].errCount = 1;
            else self.playerControlMessageLog[msg].errCount += 1;
            if (self.playerControlMessageLog[msg].errCount < 20) {
                console.log("Message not delivered yet, resending:", val.details.msgid, val.name, val);
                setTimeout(function () {
                    player.onPlayerControlMessage(val, self.playerControlMessageLog[msg].errCount > 0);
                }, 50);
            } else {
                console.log("Message resend counter exceeded, giving up:", val.details.msgid, val.name, val);
                delete self.playerControlMessageLog[msg];
            }
        }
    };

    this.onPlayerControlMessage = function (data, retrySend) {
        if (!controlMessageCheckerJob) {
            controlMessageCheckerJob = setInterval(playerControlMessageChecker, 500);
        }
        // rpc messages are disregarded
        if (typeof(data) === 'undefined' || typeof(data.details) === 'undefined' || data.rpc) {
            console.log("Player Control Message discarded, data:", data, this);
            return;
        }
        if (data.name === "StartContentPlayback" || data.name === 'SlideChanged' || data.name === 'ReloadCurrentSlide') {
            if (!retrySend) {
                player.setCurrentSlide(data.details.URL);
                resetWatchdog(data.details.duration);
                player.restartCurrentSlideTimeout(data.details.duration);
            }
        }
        if (typeof(data.details["msgid"]) === 'undefined') {
            var id = "cmd-" + new Date().getTime();
            data.details["msgid"] = id;
            self.playerControlMessageLog[id] = data;
        }
        player.sendMessage(data);
    };

    this.getZoneConfig = function () {
        return loader.slideConfig["zones"];
    };
    this.onErrorOccurred = function (errorDetails) {
        console.log("Error occured:", errorDetails);
        if (errorDetails.restartInfomonitor) {
            if (restartInProgress)return;
            restartInProgress = true;
            //lets make sure that the process restarts
            console.log("onErrorOccured, forcibly restarting Infomonitor, error:", errorDetails);
            setTimeout(function () {
                restartPlayer();
            }, 1000);
            return;
        }

        if (errorDetails.error && (errorDetails.error == 'net::ERR_ABORTED' || errorDetails.error == 'net::ERR_CONNECTION_ABORTED')) {
            //console.log("Got error net::ERR_ABORTED, this error is skipped for now( due to Chrome misfires of this error)!", errorDetails);
            return;
        }
        if (settings.get("offlinemode")) {
            connectionHealthChecker.setOffline(errorDetails.url, errorDetails.tabId);
            console.log("Got error:", errorDetails.error, ". Offline mode is active, checking node status", errorDetails.url);
            oStorage.getStatus(errorDetails.url, function (url, expired) {
                if (!expired) {
                    console.log("Offline mode active and content in offline storage, reloading url", errorDetails.url);
                    player.reloadFrame(errorDetails.url, true);
                } else {
                    console.log("Content not in offline storage, removing...");
                    loader.shouldReloadConfig = true;
                    player.execFrontEndFunction("player.removeSlide", {url: errorDetails.url});
                }
            });

        } else {
            console.log("Offline mode not active, giving up...", settings.get("offlinemode"));
        }
    };
    this.getJSInjection=function(url) {
        var injectors=this.getConfig()["injectors"];

        if(typeof(injectors)==='undefined') {
            console.log("No injectors defined!");
            return null;
        }
        var js=injectors["javascript"];
        if(typeof(js)==='undefined') {
            console.log("No Javascript injectors defined!");
            return null;
        }

        for(var i=0; i<js.length;i++) {
            var rule = js[i];
            var urlPattern = rule["urlpattern"];
            if (typeof(urlPattern) === 'undefined')continue;
            var re = new RegExp(urlPattern, "i");
            if (re.test(url)) {
                console.log("Javascript injector matches current url:"+url);
                var jss = rule["jsurl"];
                if (typeof(jss) === 'undefined') {

                    jss = rule["js"];
                }
                if(typeof(jss)!=='undefined' && jss.trim().length>0) {
                    console.log("Returning JS injector:"+jss);
                    return jss;
                }
            }
        }
        return null;
    };
    this.getCSSInjection=function(url) {
        var injectors=this.getConfig()["injectors"];

        if(typeof(injectors)==='undefined') {
            console.log("No injectors defined!");
            return null;
        }
        var ss=injectors["stylesheet"];
        if(typeof(ss)==='undefined') {
            console.log("No stylesheet injectors defined!");
            return null;
        }

        for(var i=0; i<ss.length;i++) {
            var rule = ss[i];
            var urlPattern = rule["urlpattern"];
            if (typeof(urlPattern) === 'undefined')continue;
            var re = new RegExp(urlPattern, "i");
            if (re.test(url)) {
                console.log("Stylesheet injector matches current url:"+url);
                var ssc = rule["cssurl"];
                if (typeof(ssc) === 'undefined') {
                    ssc = rule["css"];
                }
                if(typeof(ssc)!=='undefined' && ssc.trim().length>0) {
                    console.log("Returning stylesheet injector:"+ssc);

                    return ssc;
                }
            }
        }
        return null;
    };
    this.authRequestHandler=function(details) {
        var url=details.url;
        var injectors=self.getConfig()["injectors"];

        if(typeof(injectors)==='undefined') {
            console.log("No injectors defined!");
            return null;
        }
        var ba=injectors["basic_auth"];
        if(typeof(ba)==='undefined') {
            console.log("No basic authentication credentials defined!");
            return null;
        }

        for(var i=0; i<ba.length;i++) {
            var rule = ba[i];
            var urlPattern = rule["urlpattern"];
            if (typeof(urlPattern) === 'undefined')continue;
            var re = new RegExp(urlPattern, "i");
            if (re.test(url)) {
                console.log("Basic authentication credentials matches current url:", url);
                var un = rule["username"];
                var up = rule["password"];
                if(typeof(un)!=='undefined' && un.trim().length>0) {
                    return {authCredentials:{username:un, password:up}};
                }
            }
            console.log("No matching credentials found for url",url);
            return {cancel:true};
        }
    }
}
var playerMessageHandler = function (data, from, responseCallback) {
    switch (data.name) {
        case 'PreloaderEnabled':
            break;
        case 'PlayerRequestACK':
            //console.log("Sending message ACK");
            chrome.tabs.executeScript(from.tab.id, {code: playerRequestAck, allFrames: true});
            break;
        case 'PlayerControlMessageACK':
            //console.log("Received Control Message ACK:",from,data);
            delete player.playerControlMessageLog[data.msgid];
            break;

        case 'ErrorNotPossible':
            errorPossible = false;
            break;
        case 'TabErrorNotPossible':
            clearActiveTabError(from.tab);
            break;
        case 'JS':
            responseCallback(player.getJSInjection(data.url));
            break;
        case 'CustomCSS':
            responseCallback(player.getCSSInjection(data.url, from.tab));
            break;
        case 'PluginCrashed':
            responseCallback(pluginCrashHandler());
            break;
        case 'ChangeSlide':
            if (cleanUpSlideURL(data.url) == cleanUpSlideURL(player.getCurrentSlide()) || player.isExternalEventSource(data.url)) {
                player.restartCurrentSlideTimeout(30000);
                console.log("player.js: Got ChangeSlide event, next slide, current:", player.getCurrentSlide(), "target:", data.url);
                player.execFrontEndFunction("player.showNextSlide");
            }
            break;
        case 'ChangePlaybackDuration':
        case 'PlaybackDurationChange':
            player.onPlaybackDurationChange(data.details);
            if (responseCallback) responseCallback();
            break;
        case 'ZoneEventsInit':
            enableZoneEvents(data, from);
            if (responseCallback) responseCallback();
            break;
        case 'ZoneConfig':
            responseCallback(player.getZoneConfig());
            break;
        case 'LastPageReached':
            if (cleanUpSlideURL(data.url) == cleanUpSlideURL(player.getCurrentSlide())) {
                console.log("TODO: Chrome Player LastPageReached");
            }
            break;
        default:
    }
};

function attachChromePlayerListener() {
    try {
        chrome.extension.onMessage.removeListener(playerMessageHandler);
    } catch (e) {
    }
    chrome.extension.onMessage.addListener(playerMessageHandler);
}
