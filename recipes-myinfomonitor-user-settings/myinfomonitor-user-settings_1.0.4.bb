SUMMARY = "myinfomonitor User recipe"
DESCRIPTION = "This recipe adds myinfomonitor user and user config including access keys"
LICENSE = "MIT"

LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=4d92cd373abda3937c2bc47fbc49d690 \
                    file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

# files to be installed in the user directory

#SRC_URI = "file://file1 \
#           file://file2 \
#           file://file3 \
#           file://file4"
SRC_URI = "file://files.tar.gz;subdir=myinfomonitor-user-settings"
SRC_URI[md5sum] = ""
SRC_URI[sha256sum] = ""

S = "${WORKDIR}/${PN}"

inherit useradd

# You must set USERADD_PACKAGES when you inherit useradd. This
# lists which output packages will include the user/group
# creation code.
# USERADD_PACKAGES = "${PN} ${PN}-user3"
USERADD_PACKAGES = "${PN}"

# You must also set USERADD_PARAM and/or GROUPADD_PARAM when
# you inherit useradd.

# USERADD_PARAM specifies command line options to pass to the
# useradd command. Multiple users can be created by separating
# the commands with a semicolon.

USERADD_PARAM_${PN} = "-u 2010 -d /home/infomonitor -g infomonitor -G video,audio -r -s /bin/bash infomonitor"

# GROUPADD_PARAM works the same way, which you set to the options
# you'd normally pass to the groupadd command. This will create
# groups group1 and group2:
#GROUPADD_PARAM_${PN} = "-g 880 group1; -g 890 group2"

GROUPADD_PARAM_${PN} = "-g 2010 infomonitor"

# Likewise, we'll manage group3 in the useradd-example-user3 package:
#GROUPADD_PARAM_${PN}-user3 = "-g 900 group3"



do_install () {
	tar -v --no-same-owner --exclude='./patches' --exclude='./.pc' -cpvf - . \ | tar --no-same-owner -xpvf - -C ${D}
	chown -R infomonitor ${D}/home/infomonitor
	chgrp -R infomonitor ${D}/home/infomonitor
}

FILES_${PN} = "/home/infomonitor /etc/X11/Xinit.d/56disableScreenSaver /etc/X11/xorg.conf.d/00-keyboard.conf /etc/locale.conf /lib/udev/hwdb.d/70-custom-keyboard.hwdb"

# Prevents do_package failures with:
# debugsources.list: No such file or directory:
# INHIBIT_PACKAGE_DEBUG_SPLIT = "1"
